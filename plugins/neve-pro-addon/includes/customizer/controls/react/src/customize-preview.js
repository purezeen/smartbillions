/* global wp, NeveProperties */
window.addEventListener('load', () => {
  const { masonry } = NeveProperties
  if (!masonry || masonry === false) {
    return false
  }

  const api = wp.customize
  const masonryTriggers = [
    'neve_blog_covers_min_height',
    'neve_blog_grid_spacing',
    'neve_blog_separator_width',
    'neve_blog_content_padding'
  ]

  let timeout
  masonryTriggers.forEach(item => {
    const control = api(item)
    if (typeof control === 'undefined') {
      return false
    }
    control.bind(newValue => {
      clearTimeout(timeout)
      timeout = setTimeout(() => {
        window.nvMasonry.layout()
      }, 500)
    })
  })
})
