function run_scroll() {
	window.scrollTo(
		{
			top: 0,
			behavior: 'smooth'
		}
	);
}
function scroll_to_top() {
	let element = document.getElementById( 'scroll-to-top' );
	if ( ! element ) {
		return false;
	}

	element.addEventListener(
		'click',
		function() {
			run_scroll();
		}
	);

	element.addEventListener(
		'keydown',
		function ( event) {
			if ( event.key === 'Enter' ) {
				run_scroll();
			}
		}
	);

	window.addEventListener(
		'scroll',
		function() {
			let y_scroll_pos = window.pageYOffset;
			let offset       = scrollOffset.offset;

			if ( y_scroll_pos > offset ) {
				element.style.visibility = 'visible';
				element.style.opacity    = '1';
			}
			if ( y_scroll_pos <= offset ) {
				element.style.opacity    = '0';
				element.style.visibility = 'hidden';
			}


			// Change scroll to top position if there is a sticky add to cart in place.
			const stickyAddToCart = document.querySelector( '.sticky-add-to-cart-bottom' );
			if ( stickyAddToCart ) {
				element.style.bottom = '30px';

				// Try to get Neve's sticky footer. If it doesn't exist try to get Elementor's.
				let stickyFooter = document.querySelector( '.hfg_footer');
				if ( ! stickyFooter || ! stickyFooter.classList.contains( 'has-sticky-rows' ) ){
					stickyFooter = document.querySelector( '.elementor-location-footer .elementor-sticky');
				}
				const footerHeight = stickyFooter ? stickyFooter.offsetHeight : 0;

				if ( stickyAddToCart.classList.contains( 'sticky-add-to-cart--active' ) ){
					element.style.bottom = ( stickyAddToCart.offsetHeight + footerHeight + 10 ) + 'px';
				}
			}
		}
	);
}

window.addEventListener(
	'load',
	function() {
		scroll_to_top();
	}
);
