const isInViewport = function (elem) {
	const bounding = elem.getBoundingClientRect();
	return (
		bounding.top >= 0 &&
		bounding.left >= 0 &&
		bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
		bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
	);
};

function docReady(fn) {
	// see if DOM is already available
	if (document.readyState === 'complete' || document.readyState === 'interactive') {
		// call on next available tick
		setTimeout(fn, 1);
	} else {
		document.addEventListener('DOMContentLoaded', fn);
	}
}

function stickyAddToCart() {
	docReady(function () {
			addActiveClass();
			fixTopPositioning();
			fixBottomPositioning();
		}
	);
}

/**
 * Add active class on sticky add to cart when WooCommece's add to cart buttons are not in viewport.
 * @returns {boolean}
 */
function addActiveClass() {
	const addToCartButton = document.querySelector('.single_add_to_cart_button');
	const stickyAddToCart = document.querySelector( '.sticky-add-to-cart' );
	const stickyAddToCartBottom = document.querySelector( '.sticky-add-to-cart-bottom' );

	// Bail if there is no add to cart button or the sticky add to cart wrapper.
	if ( ! addToCartButton || ! stickyAddToCart ){
		return false;
	}

	window.addEventListener( 'scroll', function () {

		//Remove the active class and body bottom padding and see if it's needed again
		stickyAddToCart.classList.remove( 'sticky-add-to-cart--active' );

		//Don't add the active class if sticky add to cart is disabled on desktop and the viewport is on desktop
		if( window.innerWidth >= 960 && stickyAddToCart.classList.contains( 'not-on-desktop' ) ){
			document.body.style.paddingBottom = '0';
			return false;
		}

		//Don't add the active class if sticky add to cart is disabled on mobile and the viewport is on mobile
		if( window.innerWidth < 960 && stickyAddToCart.classList.contains( 'not-on-mobile' ) ){
			document.body.style.paddingBottom = '0';
			return false;
		}

		//Show the sticky add to cart if the WooCommerce add to cart button is not in viewport.
		if ( ! isInViewport( addToCartButton ) ) {
			stickyAddToCart.classList.add( 'sticky-add-to-cart--active' );

			// Try to get neve footer. If it's not there or it's not sticky, try to get elementor's sticky footer
			let stickyFooter = document.querySelector( '.hfg_footer');
			if ( ! stickyFooter || ! stickyFooter.classList.contains( 'has-sticky-rows' ) ){
				stickyFooter = document.querySelector( '.elementor-location-footer .elementor-sticky');
			}

			// Add the padding on body only if there is no sticky footer
			if ( stickyAddToCartBottom && ! stickyFooter ) {
				document.body.style.paddingBottom = stickyAddToCart.offsetHeight + 'px';
			}
		}
	} );
}

/**
 * Fix the top positioning if Neve has sticky header and the sticky add to cart is placed on top.
 */
function fixTopPositioning() {
	// Bail if sticky add to cart is not placed on the top of the page.
	const stickyAddToCart = document.querySelector( '.sticky-add-to-cart-top' );
	if ( ! stickyAddToCart ){
		return false;
	}

	// Try to get Neve's sticky header. If it doesn't exist try to get Elementor's.
	let stickyNav = document.querySelector( '.hfg_header');
	if ( ! stickyNav || ( ! stickyNav.classList.contains( 'has-sticky-rows--mobile' ) && ! stickyNav.classList.contains( 'has-sticky-rows--desktop' ) ) ) {
		const elementorStickyNav = document.querySelector( '.elementor-location-header .elementor-sticky');
		if ( elementorStickyNav ) {
			stickyNav = elementorStickyNav;
		}
	}

	// Calculate the position where the header should be placed
	setStickyAddToCartTop( stickyNav, stickyAddToCart );
	window.addEventListener('resize', function () {
		setStickyAddToCartTop( stickyNav, stickyAddToCart );
	});
	window.addEventListener('scroll', function () {
		setStickyAddToCartTop( stickyNav, stickyAddToCart );
	});
}

/**
 * Calculate the top position of sticky add to cart in case we have sticky header.
 */
function setStickyAddToCartTop( stickyNav, stickyAddToCart ) {

	// Check if nav is sticky and take its height in consideration if it is.
	let navHeight =  stickyNav ? stickyNav.offsetHeight : 0;
	const doc = document.documentElement;

	if( window.innerWidth < 960 && ! stickyNav.classList.contains( 'has-sticky-rows--mobile' ) ){
		navHeight = 0;
	}

	if( window.innerWidth >= 960 && ! stickyNav.classList.contains( 'has-sticky-rows--desktop' ) ){
		navHeight = 0;
	}

	// If navHeight is 0 it means that the header is not sticky. If it's not sticky we need to place the sticky
	// add to cart after the nav when nav is visible so it won't get in front of the nav.
	const top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
	const isVisible = ( top - stickyNav.offsetHeight ) < 0;
	if ( navHeight === 0 && isVisible ){
		navHeight = stickyNav.offsetHeight - top;
	}

	// Check if admin bar is present and sticky and take its height in consideration too if it is.
	const adminBar = document.getElementById( 'wpadminbar');
	const adminBarHeight = adminBar ? ( adminBar.offsetHeight - 1 ) : 0;

	stickyAddToCart.style.top = ( navHeight + adminBarHeight ) + 'px';
}

/**
 * Fix the top positioning if Neve has sticky footer and the sticky add to cart is placed on bottom.
 */
function fixBottomPositioning() {
	// Bail if sticky add to cart is not placed on the bottom of the page.
	const stickyAddToCart = document.querySelector( '.sticky-add-to-cart-bottom' );
	if ( ! stickyAddToCart ){
		return false;
	}

	// Try to get Neve's sticky footer. If it doesn't exist try to get Elementor's.
	let stickyFooter = document.querySelector( '.hfg_footer');
	if ( ! stickyFooter || ! stickyFooter.classList.contains( 'has-sticky-rows' ) ){
		stickyFooter = document.querySelector( '.elementor-location-footer .elementor-sticky');
	}

	// Calculate the position where the footer should be placed
	setStickyAddToCartBottom( stickyFooter, stickyAddToCart );
	window.addEventListener('resize', function () {
		setStickyAddToCartBottom( stickyFooter, stickyAddToCart );
	});
	window.addEventListener('scroll', function () {
		setStickyAddToCartBottom( stickyFooter, stickyAddToCart );
	});
}

/**
 * Calculate the bottom position of sticky add to cart in case we have sticky footer.
 */
function setStickyAddToCartBottom( stickyFooter, stickyAddToCart ) {
	const footerHeight =  stickyFooter ? stickyFooter.offsetHeight : 0;
	stickyAddToCart.style.bottom = footerHeight + 'px';
}

export {
	stickyAddToCart
};
