export const checkoutSteppedNav = function () {
	if ( ! document.body.classList.contains('nv-checkout-layout-stepped') ) {
		return false;
	}

	const navSteps = document.querySelectorAll( '.nv-checkout-step' );
	for (let i = 0; i < navSteps.length; i++) {
		navSteps[i].addEventListener('click', function( event ) {
			event.preventDefault();
			selectStep( this );
		});
	}

	const nextStepButton = document.querySelectorAll( '.next-step-button-wrapper .button' );
	for (let i = 0; i < navSteps.length; i++) {
		if ( typeof nextStepButton[i] !== 'undefined' ){
			nextStepButton[i].addEventListener( 'click', function ( event) {
				event.preventDefault();
				selectStep( this );
				document.getElementById('content').scrollIntoView({
					behavior: 'smooth'
				});
			});
		}
	}
};

function selectStep( step ) {
	let navSteps = document.querySelectorAll( '.nv-checkout-step' );
	let currentStep = step.dataset.step;
	const currentStepIndex = currentStep === 'billing' ? 0 : ( currentStep === 'review' ? 1 : 2 );

	for (let i = 0; i < navSteps.length; i++) {
		navSteps[i].classList.remove( 'active' );
		if ( i <= currentStepIndex ){
			navSteps[i].classList.add( 'active' );
		}
	}

	const customerDetails = document.getElementById( 'customer_details' );
	const customerDetailsNextButton = document.querySelector( '.next-step-button-wrapper.billing' );
	const shopTable = document.querySelector( '.shop_table' );
	const shopTableNextButton = document.querySelector( '.next-step-button-wrapper.review' );
	const payment = document.getElementById( 'payment' );

	if ( currentStep === 'billing' ) {
		shopTable.style.display = 'none';
		shopTableNextButton.style.display = 'none';
		payment.style.display = 'none';
		customerDetails.style.display = 'block';
		customerDetailsNextButton.style.display = 'block';
	}

	if ( currentStep === 'review' ) {
		shopTable.style.display = 'table';
		shopTableNextButton.style.display = 'block';
		payment.style.display = 'none';
		customerDetails.style.display = 'none';
		customerDetailsNextButton.style.display = 'none';
	}

	if ( currentStep === 'payment' ) {
		shopTable.style.display = 'none';
		shopTableNextButton.style.display = 'none';
		payment.style.display = 'block';
		customerDetails.style.display = 'none';
		customerDetailsNextButton.style.display = 'none';
	}
}

