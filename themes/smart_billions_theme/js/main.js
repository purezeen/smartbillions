(function($) {

    $(document).ready(function() {

        // ---------------------------CAROUSEL- Home page
        $('.carousel').slick({
            dots: false,
            arrows: false,
            infinite: true,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 4,
            // autoplay: true,
            // autoplaySpeed: 4000,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                        centerMode: true
                    }
                }

            ]
        });

        // --------------------------- CAROUSEL MORE
        $('.carousel-more').slick({
            dots: false,
            arrows: false,
            speed: 300,
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            responsive: [{
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: false,
                        centerMode: true
                    }
                }

            ]
        });



        // ----------------------------------- dropdown menu - logo
        if ($(window).width() < 992) {

            $(document).on("click", function(event) {
                if (!$(event.target).closest(".site-branding").length) {
                    $(".logo-menu").slideUp(200);
                    $('.logo .arrow-down').removeClass("up");
                }
            });

            $('.header .logo .arrow-down').on("click", function(e) {

                e.preventDefault();
                e.stopPropagation();

                if ($(this).hasClass("up")) {
                    $(this).removeClass("up");
                    $(this).parent()
                        .siblings(".logo-menu")
                        .slideUp(200);

                } else {

                    $(this).addClass("up");

                    $(this).parent()
                        .siblings(".logo-menu")
                        .slideDown(200);
                }

            });

        } else {

            $('.header .logo > a, .header .logo-menu').hover(

                function() {
                    //show its sibling menu
                    $('.header .logo-btn').addClass('hover');
                    $('.header .logo-menu').stop().slideDown();
                },

                function() {
                    //hide its sibling menu
                    $('.header .logo-btn').removeClass('hover');
                    $('.header .logo-menu').stop().slideUp();
                });
        }

        // ---------------------------------------------------- ACCORDION
        $(".accordion-item > button").on("click", function() {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this)
                    .siblings(".accordion-content")
                    .slideUp(200);

                $(".accordion-item > button span")
                    .removeClass("up")
                    .addClass("down");

            } else {
                $(".accordion-item > button span")
                    .removeClass("up")
                    .addClass("down");
                $(this)
                    .find("span")
                    .removeClass("down")
                    .addClass("up");

                $(".accordion-item > button").removeClass("active");
                $(this).addClass("active");
                $(".accordion-content").slideUp(200);
                $(this)
                    .siblings(".accordion-content")
                    .slideDown(200);
            }
        });


        // --------------------------------------STICKY FOOTER
        if ($('.info-footer').length) {
            $(window).on('scroll', function() {
                var element = $('.single-wallets .title-box');
                var height = element.outerHeight();
                var offsetTop = element.offset().top;

                if ($(window).scrollTop() > height + offsetTop) {
                    $('.info-footer').slideDown(500);
                } else {
                    $('.info-footer').slideUp(500);
                }
            });
        }


        // -------------------------------------------SINGLE LEARN PAGE - TABLE
        var table = $('.crypto-table table').DataTable({
            scrollY: "300px",
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            paging: false,
            searching: false,
            ordering: false,
            info: false,
            fixedColumns: {
                leftColumns: 1
            }
        });

        // ------------------------------------------------ auto resize textarea
        $('textarea').each(function() {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        }).on('input', function() {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });

        //------------------------------------------------------CONTACT FORM ANIMATION
        $(".form input:not([type=submit]), .form textarea").on("blur input focus", function() {
            var $field = $(this).parent().parent();
            if (this.value) {
                $field.addClass("label-anima");
            } else {
                $field.removeClass("label-anima");
            }
        });

        $(".form input:not([type=submit]), .form textarea").on("focus", function() {
            var $field = $(this).parent().parent();
            if (this) {
                $field.addClass("label-anima");
            } else {
                $field.removeClass("label-anima");
            }
        });

        // Compare page 
        $('.parent_wrap').on('click', '.parent_item', function(e) {
            e.preventDefault();
            $('.all-comparisons').hide();
            $('.parent_wrap').hide();

            // var activeEl = $(this).closest('.parent_item');
            parentItem = $(this).attr("data-cat");

            var childItem = $('.child-wrap').attr("data-cat");

            $('.child-wrap').hide();
            $('.child-wrap[data-cat=' + parentItem + ']').fadeIn('500');
        });

        $('.child-wrap .js-filter-item > a').on('click', function(e) {
            $('.child-wrap').fadeOut();
            $('.compare-title').hide();
        });

        $('.parent_only').on('click', function(e) {
            $('.parent_wrap').hide();
            $('.compare-title').hide();
        });

        //  ------------------------------ TOOLTIP TOUCHSTART 
        $('.tooltip-wrap').on('touchstart', function(e) {
            e.preventDefault();
            $('.tooltip-wrap').removeClass('hover');
            $(this).addClass('hover');
        });

        $(document).on("touchstart", function(event) {
            if (!$(event.target).closest(".tooltip-wrap").length) {
                $(".tooltip-wrap").removeClass("hover");
            }
        });


        // ------------------------------------------------------------------ DROPDOWN MENU
        $(".navigation-menu > .menu-item-has-children > .sub-menu").wrap("<div class='sub-menu--wrap'></div>");
        $(".navigation-menu > .menu-item-has-children > a, .buyCoins > a, .countryGuides > a ").append("<span class='round-arrow-down'></span>");
        $(".sub-menu--wrap > .sub-menu ").prepend("<li class='title'>  <span class='close'></span></li>");

        var dropdownCoins = $('#dropdown-coins-content');
        var dropdownCountry = $('#dropdown-country-content');
        var dropdownBtn = $('.buyCoins > a span');
        var dropdownBtn1 = $('.countryGuides > a span');

        if ($(window).width() < 990) {

            $('.sub-menu >.menu-item-has-children > a').append("<span class='round-arrow-down'></span>");

            var close = $(".header .close");
            var body = $('body');

            close.on("click", function() {
                $(".navigation-menu > li > a").removeClass("active");
                $(".navigation-menu  .sub-menu--wrap  .sub-menu").slideUp();
                $(".navigation-menu > li > a span").removeClass("up");
                dropdownCoins.slideUp();
                dropdownCountry.slideUp();
                $(".header").removeClass('mobile-bg');

                if (body.hasClass('noscroll')) {
                    body.removeClass('noscroll');
                }
            });


            $('.navigation-menu > .no-link > a').on("click", function(e) {
                e.preventDefault();
                e.stopPropagation();

            });

            $('.navigation-menu > .menu-item-has-children > a span').on("click", function(e) {
                e.preventDefault();
                e.stopPropagation();

                $('html').scrollTop(0);

                if ($(this).parent().hasClass("active")) {
                    $(".header").removeClass('mobile-bg');

                    $(this).parent().removeClass("active");
                    $(this).parent()
                        .siblings(".sub-menu--wrap")
                        .slideUp(300, 'linear');

                    $(this).parent()
                        .siblings(".sub-menu--wrap").find('.sub-menu').fadeOut(200);

                    $(".navigation-menu > li > a span")
                        .removeClass("up");

                    body.removeClass('noscroll');

                } else {
                    $(".navigation-menu > li > a span").removeClass("up");
                    $(this).addClass("up");

                    var link_value = $(this).parent().clone().children().remove().end().text();
                    $(this).parent().next('.sub-menu--wrap')
                        .find('.title').contents().first().replaceWith(link_value);

                    $(".navigation-menu > .menu-item-has-children > a, .buyCoins > a, .countryGuides > a").removeClass("active");
                    $(this).parent().addClass("active");
                    $(".header").addClass('mobile-bg');
                    $(".navigation-menu > .menu-item-has-children > .sub-menu--wrap").slideUp(300, 'linear');
                    $(".navigation-menu > .menu-item-has-children > .sub-menu--wrap >.sub-menu").fadeOut(300);
                    dropdownCoins.slideUp(300, 'linear');
                    dropdownCountry.slideUp(300, 'linear');
                    $(this).parent()
                        .siblings(".sub-menu--wrap")
                        .fadeIn('fast');

                    $(this).parent()
                        .siblings(".sub-menu--wrap").find('> .sub-menu').delay(200).slideDown(300, 'linear');

                    body.addClass('noscroll');

                }

            });

            $('.sub-menu .menu-item-has-children > a > span').on("click", function(e) {

                e.preventDefault();
                e.stopPropagation();

                if ($(this).parent().hasClass("active")) {
                    $(this).parent().removeClass("active");
                    $(this).parent()
                        .siblings(".sub-menu")
                        .slideUp(200);

                    $(".sub-menu .menu-item-has-children > a span")
                        .removeClass("up");

                } else {
                    $(".sub-menu .menu-item-has-children > a span")
                        .removeClass("up");
                    $(this)
                        .addClass("up");

                    $(".sub-menu .menu-item-has-children > a").removeClass("active");
                    $(this).parent().addClass("active");
                    $(".sub-menu .menu-item-has-children > .sub-menu").slideUp(200);
                    $(this).parent()
                        .siblings(".sub-menu")
                        .slideDown(200);

                }

            });

        
             dropdownBtn.on("click", function(e) {
                e.preventDefault();
                e.stopPropagation();

                $('html').scrollTop(0);
                
                if ( $(this).parent().hasClass("active")) {
                    $(".header").removeClass('mobile-bg');

                    $(this).parent().removeClass("active");
                    dropdownCoins.slideUp(400, 'linear');

                    $(".navigation-menu > li > a span")
                        .removeClass("up");

                    body.removeClass('noscroll');

                } else {
                    $(".navigation-menu > li > a span").removeClass("up");
                    $(this).addClass("up");

                    $(".navigation-menu > li > a,  .countryGuides > a").removeClass("active");
                    $(this).parent().addClass("active");
                    $(".header").addClass('mobile-bg');
                    $(".navigation-menu > .menu-item-has-children > .sub-menu--wrap").slideUp(300, 'linear');
                    $(".navigation-menu > .menu-item-has-children > .sub-menu--wrap >.sub-menu").fadeOut(300);
                    dropdownCountry.slideUp(400);
                    dropdownCoins.slideDown(400);

                    body.addClass('noscroll');

                }

            });

            dropdownBtn1.on("click", function(e) {
                e.preventDefault();
                e.stopPropagation();
                $('html').scrollTop(0); 

                if ( $(this).parent().hasClass("active")) {
                    $(".header").removeClass('mobile-bg');

                    $(this).parent().removeClass("active");
                    dropdownCountry.slideUp(400, 'linear');

                    $(".navigation-menu > li > a span")
                        .removeClass("up");

                    body.removeClass('noscroll');

                } else {
                    $(".navigation-menu > li > a span").removeClass("up");
                    $(this).addClass("up");

                    $(".navigation-menu > li > a, .buyCoins > a").removeClass("active");
                    $(this).parent().addClass("active");
                    $(".header").addClass('mobile-bg');

                    $(".navigation-menu > .menu-item-has-children > .sub-menu--wrap").slideUp(300, 'linear');
                    $(".navigation-menu > .menu-item-has-children > .sub-menu--wrap >.sub-menu").fadeOut(300);
                    dropdownCoins.slideUp(400);
                    dropdownCountry.slideDown(400);

                    body.addClass('noscroll');

                }

            });

        } else {

            $(".sub-menu--wrap > .sub-menu > li > a ").append("<span class='round-arrow-right'></span>");

            $('.menu-item-has-children.no-link > a').on("click", function(e) {
                e.stopPropagation();
                e.preventDefault();
            });

              // hover event
              $('.buyCoins, #dropdown-coins-content').hover(

                function() {
                    //show its sibling menu
                    $('.buyCoins').addClass('hover');
                    dropdownCoins.addClass('hover');
                    
                },

                function() {
                    //hide its sibling menu
                    $('.buyCoins').removeClass('hover');
                    dropdownCoins.removeClass('hover');
                   
                });


            $('.countryGuides, #dropdown-country-content').hover(

                function() {
                    //show its sibling menu
                    $('.countryGuides').addClass('hover');
                    dropdownCountry.addClass('hover');
                    
                },

                function() {
                    //hide its sibling menu
                    $('.countryGuides').removeClass('hover');
                    dropdownCountry.removeClass('hover');
                    
                });

        }


        // ------------------------------------------SMOOTH SCROLL 
        $('a[href*="#"]')
            // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function(event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });



        // ----------navigation - bold links on hover
        $(".nav a").each(function() {
            var val = $(this).text();
            $(this).attr('data-text', val);
        });


    });



})(jQuery);