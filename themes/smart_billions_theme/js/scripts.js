(function($) {

   $(document).ready( function() {
      $(document).on('click', '.js-filter-item > a', function(e) {
         e.preventDefault();
         
         var category = $(this).data('category');
         var child = $(this).data('child');
         $.ajax({
            url : wpAjax.ajaxUrl,
            data : {action: "filter", category: category, child: child },
            type : "post",
            success: function(result) {
               $('.js-filter').html(result);
            },
            error: function(result) {
               console.warn(result);
            }
         }); 
      });
   });

})(jQuery);