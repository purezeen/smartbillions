<?php /* Template Name: Crypto */ get_header(); ?>

<main class="crypto">
    <?php 
    // Get Learn page ID by his ancestor
    // $childrens = get_pages( array( 'child_of' => get_the_ID() ) );
    // foreach ($childrens as $children) {
     // if ($children->post_name == 'learn') {
         // $post_parent = $children->ID;
     // }
    // }
    $guide_list = get_field( 'guide_list' );
    $args = array(
        'post_parent' => $guide_list,
        // 'child_of' => get_the_ID(),
        'post_type'   => 'page',
        'posts_per_page' => 4,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post_status' => 'publish' ,
        'category_name' => 'guide+learn',
        );
    $guide_query = new WP_Query($args);
    ?>

    <?php if ($guide_query->have_posts()) : ?>
    <div class="crypto-head">
        <div class="container">
            <h1><?php the_field( 'section_title_for_guides' ); ?></h1>
            <div class="carousel">
            <?php $i = 1; ?>
            <?php while ($guide_query->have_posts()) : $guide_query->the_post(); ?>
                <a href="<?php the_permalink(); ?>" class="item"
                    style="background-image:url(../wp-content/themes/smart_billions_theme/img/slider-mask<?php echo $i; ?>.svg)">
                    <button class="button-round-white">Part <?php the_field( 'number_of_part' ); ?></button>
                    <h3><?php the_title(); ?></h3>
                </a>
                <?php $i++; ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php if ( have_rows( 'guides_to_cryptocurrency' ) ) : ?>
    <div class="crypto-guides">
        <div class="container">
            <div class="row">
            <?php $count_column = 1; ?>
            <?php while ( have_rows( 'guides_to_cryptocurrency' ) ) : the_row(); ?>
                <div class="col-lg-4">
                    <div class="crypto-guide <?php echo ($count_column == 3) ? 'reviews' : ''?>">
                        <div class="caption">
                            <h2><?php the_sub_field( 'guide_title' ); ?></h2>
                            <div class="round-icon">
                                <img src="<?php echo get_template_directory_uri() ?>/img/<?php the_sub_field( 'guide_icon' ); ?>-icon.svg"
                                    alt="<?php the_sub_field( 'guide_icon' ); ?>-icon">
                            </div>
                        </div>
                    <?php if ( have_rows( 'guide_links' ) ) : ?>
                        <ul>
                        <?php while ( have_rows( 'guide_links' ) ) : the_row(); ?>
                            <?php $select_guide = get_sub_field( 'select_guide' ); ?>
                            <?php if ( get_sub_field( 'enable_custom_title_input' ) == 1 ) { 
                                $guide_title = get_sub_field( 'guide_custom_title' );
                            } else { 
                                $guide_title = $select_guide->post_title;
                            } ?>
                            <?php if ( get_sub_field( 'show_rate' ) == 1 ) { ?>
                           
                            <li><a href="<?php echo get_permalink($select_guide->ID); ?>"><?php echo $guide_title; ?></a>
                                <div class="rating-wrap" style="--rating: <?php the_sub_field( 'rating' ); ?>;">
                                    <div class="rating"></div>
                                    <small><?php the_sub_field( 'rating' ); ?>/5</small>
                                </div>
                            </li>
                            <?php }  else { ?>
                            <li><a href="<?php echo get_permalink($select_guide->ID); ?>"><?php echo $guide_title; ?> <i class="arrow-right"></i></a></li>
                            <?php } ?>
                        <?php endwhile; ?>

                        </ul>
                    <?php endif; ?>
                    </div>
                </div>
            <?php $count_column++; ?>
            <?php endwhile; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php 
    $show_children_of_selected_page_parent = get_field( 'show_children_of_selected_page_parent' );
    if ($show_children_of_selected_page_parent) { ?>
        <div class="crypto-comparisons">
        <div class="container">
            <div class="caption">
                <div class="caption--title">
                    <h2><?php the_field( 'section_title' ); ?></h2>
                    <?php $link = get_field( 'link' ); ?>
                    <?php if ( $link ) { ?>
                        <a class="see-all" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><i class="arrow-right"></i><?php echo $link['title']; ?></a>
                    <?php } ?>
                </div>
                <div class="round-icon">
                    <img src="<?php echo get_template_directory_uri() ?>/img/comparisons-icon.svg"
                        alt="Comparisons-icon">
                </div>
            </div>
            <div class="row">
            <?php
            $page_childrens = get_children( 
                array( 
                    'post_parent' => $show_children_of_selected_page_parent,
                    'sort_column' => 'post_date',
                    'numberposts' => 3,
                    'post_type' => 'page',
                    'post_statis' => 'publish',
                    'sort_order' => 'desc'
                ) 
            );
             
            foreach ($page_childrens as $children) { ?>
                
                <div class="col-md-4">
                    <div class="global-item">
                        <span class="update update-desktop">Last update:
                        <?php 
                        $u_time = get_the_time('U'); 
                        $u_modified_time = get_the_modified_time('U'); 
                        if ($u_modified_time >= $u_time + 86400) { 
                        the_modified_time('M j, Y'); 
                        } else {
                            echo get_the_date();
                         }
                        ?></span>
                        <a href="<?php echo get_permalink($children->ID); ?>">
                            <h3><?php echo $children->post_title; ?></h3>
                        </a>
                        <p><?php echo get_the_excerpt( $children->ID );?></p>
                        <a href="<?php echo get_permalink($children->ID); ?>" class="button">Read more</a>
                        <div class="update-mobile">
                            <span class="update">Last update:</span>
                            <span class="update">
                            <?php 
                            $u_time = get_the_time('U'); 
                            $u_modified_time = get_the_modified_time('U'); 
                            if ($u_modified_time >= $u_time + 86400) { 
                            the_modified_time('M j, Y'); 
                            } else {
                                echo get_the_date();
                             }
                            ?></span>
                        </div>
                    </div>
                </div>
                
            <?php
            }
            ?>
            </div>
        </div>
    </div>
    <?php
    }
    ?>

   <!-- News section  -->
   <?php if ( have_rows( 'news_section' ) ) : ?>
      <div class="crypto-news">
         <div class="container">
            <?php while ( have_rows( 'news_section' ) ) : the_row(); ?>
               <div class="caption">
                  <div class="caption--title">
                     <h2><?php the_sub_field( 'section_title' ); ?></h2>
                     <?php $link = get_sub_field( 'link' ); ?>
                    <?php if ( $link ) { ?>
                        <a class="see-all" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><i class="arrow-right"></i><?php echo $link['title']; ?></a>
                    <?php } ?>
                  </div>
                  <?php $section_icon = get_sub_field( 'section_icon' ); ?>
                  <?php if ( $section_icon ) { ?>
                     <div class="round-icon">
                        <img src="<?php echo $section_icon['url']; ?>" alt="<?php echo $section_icon['alt']; ?>" />
                     </div>
                  <?php } ?>
               </div>
               <?php $choose_news_category_term = get_sub_field( 'choose_news_category' ); ?>
               <?php if ( $choose_news_category_term ): ?>
                  <?php $news_category_id = $choose_news_category_term[0]->term_id; ?>

                  <?php $wp_query = new WP_Query(); ?>
                  <?php $args = array(
                     'post_type' => 'post',
                     'cat' => $news_category_id, 
                     'showposts' => 3,
                     'orderby'    => 'modified',
                     'post_status' => 'publish'
                  ); ?>

                  <?php $wp_query->query($args); ?>
                  
                  <?php if ( $wp_query->have_posts() ) : ?>

                     <div class="row">
                        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                           <div class="col-md-4">
                              <div class="news-item">
                                <span class="update update-desktop">Last update: 
                                <?php 
                                $u_time = get_the_time('U'); 
                                $u_modified_time = get_the_modified_time('U'); 
                                if ($u_modified_time >= $u_time + 86400) { 
                                the_modified_time('M j, Y'); 
                                }else {
                                    echo get_the_date();
                                 } 
                                ?>
                                </span>
                                <a href="<?php the_permalink(); ?>">
                                    <h4><?php echo the_title(); ?></h4>
                                </a>
                                <span class="update update-mobile">
                                    <?php 
                                    $u_time = get_the_time('U'); 
                                    $u_modified_time = get_the_modified_time('U'); 
                                    if ($u_modified_time >= $u_time + 86400) { 
                                    the_modified_time('M j, Y'); 
                                    } else {
                                        echo get_the_date();
                                     }
                                    ?>
                                <span>
                              </div>
                           </div>
                        <?php endwhile; ?>
                     </div>

                  <?php endif; ?>

               <?php endif; ?>
            <?php endwhile; ?>
         </div>
      </div>
   <?php endif; ?>
   <!-- End news section  -->

</main>
<?php get_footer(); ?>