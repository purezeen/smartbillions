<?php /* Template Name: About */ get_header(); ?>
<main class="about">
    <div class="news-head wallets-head">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-1">
                    <div class="round-coin-icon">
                        <img src="../wp-content/themes/smart_billions_theme/img/Logo.svg" alt="Logo-icon">
                    </div>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="about-info">
        <div class="container">
            <div class="row">
                <?php if ( have_rows( 'who_we_are' ) ) : ?>
                    <?php while ( have_rows( 'who_we_are' ) ) : the_row(); ?>
                    <div class="col-md-6 col-xl-4 offset-xl-1">
                        <div class="content">
                            <?php the_sub_field( 'wysiwyg' ); ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
    <div class="about-team">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 offset-xl-1">
                    <h2><?php the_field( 'the_team_heading' ); ?></h2>
                </div>
                <?php if ( have_rows( 'the_team' ) ) : ?>
                    <?php while ( have_rows( 'the_team' ) ) : the_row(); ?>
                    <div class="col-md-6 col-xl-4 offset-xl-1">
                        <div class="author">
                            <div class="author-title">
                                <?php $avatar = get_sub_field( 'avatar' ); ?>
                                <?php if ( $avatar ) { ?>
                                    <img src="<?php echo $avatar['url']; ?>" alt="<?php echo $avatar['alt']; ?>" />
                                <?php } ?>
                                <h6><span><?php the_sub_field( 'role' ); ?></span> <?php the_sub_field( 'name_and_surname' ); ?></h6>
                            </div>
                            <?php the_sub_field( 'about_team_member' ); ?>
                            <?php if ( have_rows( 'social_network' ) ) : ?>
                                <?php while ( have_rows( 'social_network' ) ) : the_row(); ?>
                                    <a href="<?php the_sub_field( 'url' ); ?>" class="social-icon">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/<?php the_sub_field( 'name' ); ?>-white.svg" alt="<?php the_sub_field( 'name' ); ?>">
                                    </a>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php if ( have_rows( 'contact_form' ) ) : ?>
        <?php while ( have_rows( 'contact_form' ) ) : the_row(); ?>
        <div class="container">
            <div class="about-contact">
                <div class="row">
                    <div class="col-md-6 col-xl-5 offset-xl-1">
                        <div class="head">
                            <?php the_sub_field( 'wysiwyg' ); ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-5">
                        <?php $contact_form_shortcode = get_sub_field( 'contact_form_shortcode' ) ?>
                        <?php echo do_shortcode( $contact_form_shortcode ); ?>
                    </div>
                </div>
            </div>
        </div>
            
        <?php endwhile; ?>
    <?php endif; ?>

</main>
<?php get_footer(); ?>