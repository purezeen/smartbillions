<?php /* Template Name: Single Coin */ get_header(); ?>

<main class="single-wallets single-coin">
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-1">
                    <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                    <span class="update update-desktop update-page">Last update:
                    <?php 
                    $u_time = get_the_time('U'); 
                    $u_modified_time = get_the_modified_time('U'); 
                    if ($u_modified_time >= $u_time + 86400) { 
                    the_modified_time('M j, Y'); 
                    } else {
                        echo get_the_date();
                    }
                    ?></span>
                </div>
                <div class="col-xl-8 offset-xl-2">
                    <div class="title">
                    <?php $logo_style = get_field( 'logo_style' ); ?>
                        <?php if($logo_style == "black"):?>
                    <?php $logo_style_class = "round-icon--black"; ?>
                    <?php elseif($logo_style == "without"): ?>
                        <?php $logo_style_class = "round-icon--abra"; ?>
                        <?php elseif($logo_style == "default"): ?>
                        <?php $logo_style_class = ""; ?>
                        <?php else: ?>
                        <?php $logo_style_class = ""; ?>
                    <?php endif; ?>

                        <div class="round-coin-icon <?php echo $logo_style_class; ?>">
                            <?php $currency_image = get_field( 'currency_image' ); ?>
                            <?php if ( $currency_image ) { ?>
                                <img src="<?php echo $currency_image['url']; ?>" alt="<?php echo $currency_image['alt']; ?>" />
                            <?php } ?>
                        </div>
                        <div class="name">
                            <h1><?php the_title(); ?></h1>
                            <span><?php the_field( 'currency' ); ?></span>
                        </div>
                        <div class="price">
                            <?php if(get_field( 'price' )): ?>
                                <div class="with-line">
                                    <em>price</em>
                                    <h3><?php the_field( 'price' ); ?> 
                                        <?php if ( get_field( 'caret' ) == 1 ) { ?>
                                        <i class="price-icon-up"></i>
                                        <?php } else { ?>
                                        <i class="price-icon-down"></i> 
                                        <?php } ?>
                                    </h3>
                                </div>
                            <?php endif; ?>
                            <?php if(get_field( 'market_value' )): ?>
                            <div>
                                <em>Market value</em>
                                <h3><?php the_field( 'market_value' ); ?></h3>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <p><?php the_content(); ?></p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">
                <ul class="info">
                    <li>
                        <div class="title-box">
                            <h4><?php the_field( 'global_crypto_name' ); ?></h4>
                        </div>
                        <!-- <a target="_blank" class="brand-link" href="<?php the_field( 'official_site_url' ); ?>"><?php echo parse_url(get_field( 'official_site_url' ), PHP_URL_HOST); ?></a> -->
                        <br>
                        <?php if ( have_rows( 'additional_info' ) ) : ?>
                        <ol>
                            <?php while ( have_rows( 'additional_info' ) ) : the_row(); ?>
                            <li>
                                <small><?php the_sub_field( 'label' ); ?></small> <?php the_sub_field( 'info' ); ?>
                            </li>
                            <?php endwhile; ?>
                        </ol>
                        <?php endif; ?>
                        <?php if ( get_field( 'add_nofollow_link' ) == 1 ) { 
                            $nofollow = "rel='nofollow'";
                        } else { 
                            $nofollow = "";
                        } ?>

                        <?php if(get_field( 'button_name' )): ?>
                            <div class="buttons">
                                <a target="_blank" <?php echo $nofollow; ?> href="<?php the_field( 'official_site_url' ); ?>" class="button"><?php the_field( 'button_name' ); ?></a>
                            </div>
                        <?php else: ?>
                            <div class="buttons">
                                <a target="_blank" <?php echo $nofollow; ?> href="<?php the_field( 'official_site_url' ); ?>" class="button">Visit site</a>
                            </div>
                        <?php endif; ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>

   <!-- Include flexible single content  -->
   <?php get_template_part( 'template-parts/flexible-single', 'leftalign' ); ?>
    <!-- End flexible single content  -->

    <!-- Author section  -->
    <?php get_template_part( 'template-parts/author', 'section' ); ?>
    <!-- End author section  -->

    <!--------------------------------- SLIDER -->

    <?php
    $posttags = get_the_tags($post->ID );
    $tag_id = array();
    if ($posttags) {
        foreach($posttags as $tag) {
            $tag_id[] =  $tag->term_id; 
        }
    }
    ?>

    <?php if(!empty($tag_id)): ?>
        <?php
        $args = array(
            'post__not_in' => array(get_the_ID()),
            'post_type' => 'page',
            'posts_per_page' => -1,
            'tag__in' => $tag_id,
            'post_status' => 'publish'
        );
        ?>

        <?php $the_query = new WP_Query( $args ); ?>
            <?php if ($the_query->have_posts()) : ?>
                <div class="single-coin-carousel">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-8 offset-xl-2">
                                <h2>Articles that might interest you test</h2>
                                <div class="carousel-more">
                                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                                        <a href="<?php the_permalink(); ?>" class="item">
                                            <?php
                                                $categories = get_the_category();
                                                if ( ! empty( $categories ) ) {
                                                    $cat_id =  $categories[0]->term_id ; 

                                                    foreach($categories as $childcat) {
                                                        $parentcatid = $childcat->category_parent;
                                                        $childcat_name = $childcat->cat_name;
                                                        if($parentcatid>0){
                                                            $parentcat_name = get_cat_name($parentcatid);
                                                            continue;
                                                        }else {
                                                            $parentcat_name = "test";
                                                        }
                                                    }
                                                    ?>
                                                    <span><?php echo $parentcat_name; ?> <span><?php echo $childcat_name; ?></span></span>
                                                    <?php
                                                }
                                            ?>
                                            <h3><?php the_title(); ?></h3>
                                        </a>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    <?php endif; ?>
   

    <!---------------------------------STICKY FOOTER -->
    <div class="info-footer">
        <div class="container">
            <?php $logo_style = get_field( 'logo_style' ); ?>
            <?php if($logo_style == "black"):?>
            <?php $logo_style_class = "round-icon--black"; ?>
            <?php elseif($logo_style == "without"): ?>
                <?php $logo_style_class = "round-icon--abra"; ?>
                <?php elseif($logo_style == "default"): ?>
                <?php $logo_style_class = ""; ?>
                <?php else: ?>
                <?php $logo_style_class = ""; ?>
            <?php endif; ?>

            <div class="round-coin-icon <?php echo $logo_style_class; ?>">
               <?php if ( $currency_image ) { ?>
                    <img src="<?php echo $currency_image['url']; ?>" alt="<?php echo $currency_image['alt']; ?>" />
                <?php } ?>
            </div>
            <div class="title-wrap">
                <h4><?php the_field( 'global_crypto_name' ); ?></h4>
            </div>
            <?php if ( get_field( 'add_nofollow_link' ) == 1 ) { 
                $nofollow = "rel='nofollow'";
            } else { 
                $nofollow = "";
            } ?>

            <?php if(get_field( 'button_name' )): ?>
                <a target="_blank" <?php echo $nofollow; ?> href="<?php the_field( 'official_site_url' ); ?>" class="button"><?php the_field( 'button_name' ); ?></a>
            <?php else: ?>
                <a target="_blank" <?php echo $nofollow; ?> href="<?php the_field( 'official_site_url' ); ?>" class="button">Visit site</a>
            <?php endif; ?>
        </div>
    </div>

</main>

<?php get_footer(); ?>