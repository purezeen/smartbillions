<?php /* Template Name: Single compare */ get_header(); ?>

<main class="single-compare">

    <!-- Title and big paragraph -->
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">
                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                <span class="update update-desktop update-page">Last update:
                <?php 
                $u_time = get_the_time('U'); 
                $u_modified_time = get_the_modified_time('U'); 
                if ($u_modified_time >= $u_time + 86400) { 
                the_modified_time('M j, Y'); 
                } else {
                    echo get_the_date();
                }
                ?></span>
            </div>
            <div class="col-xl-8 offset-xl-2">
                <h1><?php the_title(); ?></h1>

                <div class="text-intro">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Include flexible single content  -->
    <?php get_template_part( 'template-parts/flexible-single', 'centeralign' ); ?>
    <!-- End flexible single content  -->

    <!-- Author section  -->
    <?php get_template_part( 'template-parts/author', 'section' ); ?>
    <!-- End author section  -->
   
    <!--------------------------------- SLIDER -->

    <!-- Editor -->
    <!-- <div class="container editor">
        <div class="row">
            <div class="col-xl-6 offset-xl-3">
                <div class="contents-list">
                    <span>contents</span>
                    <ul>
                        <li><a href="">Headline 1</a></li>
                        <li><a href="">Headline 2</a></li>
                        <li><a href="">Headline 3</a></li>
                        <li><a href="">Headline 4</a></li>
                    </ul>
                </div>

                <p>The first thing you must do is find the right trading platform for getting some Bitcoins to begin
                    with. There is Binance, Gemini, Monero, Kraken, Bittrex, among others. Now, talking about
                    cryptocurrency exchanges, Coinbase and Bitfinex are two of the industry’s most established names.
                    So, it is natural that you may be a little confused about making the right pick.</p>
                <p>Well, a coin-toss isn’t going to help you, of course. However, a brief comparison of both exchanges
                    will! To make things easier, we have precisely done that in this article. We have compared both
                    Coinbase and Bitfinex based on aspects that matter when buying a Bitcoin (BTC) or altcoins such as
                    Bitcoin Cash (BCH), Ethereum (ETH), Tron (TRX), Ripple (XRP), Litecoin (LTC), and stablecoin such as
                    Tether (USDT).</p>
                <p>We’ll start with a basic introduction to both services.</p>

                <h3>Coinbase vs Bitfinex: Credibility</h3>
                <img src="<?php echo get_template_directory_uri() ?>/img/compare-img1.png" alt="">
                <p>Founded in June 2012, Coinbase has its headquarters in San Francisco. One of the go-to names for
                    crypto trading allows users to trade cryptocurrencies. Since then, Coinbase has built a reputation
                    for being the beginner’s place for all things cryptocurrency. As of 2019, this company had a total
                    revenue of more than $1 billion.</p>
                <p>Bitfinex also made its debut in 2012, but the company is headquartered in Hong Kong. Having said
                    that, it has a registered office in the British Virgin Islands. However, compared to Coinbase,
                    Bitfinex had a not-so-stable growth due to several issues, including instances when money was stolen
                    from customers’ accounts. It has, however, managed to retain customers by offering a specific set of
                    features. Nevertheless, Bitfinex has been trying to improve its characteristics over time.</p>

                <h3>History</h3>
                <img src="<?php echo get_template_directory_uri() ?>/img/compare-img2.png" alt="">
                <p>Coinbase has had a pretty decent history as an early cryptocurrency exchange. Since its launch in
                    2012, the firm has seen an increasing number of customers, thanks to the intuitive User Interface,
                    among other things. There were a few allegations about how the company had violated thousands of
                    customers’ piracy, but Coinbase had denied all the allegations, saying that no data was lost. The
                    exchange has also been criticized for taking a massive commission from trading transactions.</p>
                <p>Bitfinex had faced even bigger problems from the beginning, though. First of all, Bitfinex is not a
                    transparent company when it comes to numbers. Because it is headquartered in Hong Kong and all the
                    legalities come under HK jurisdiction, we cannot be sure about the exchange numbers. Furthermore,
                    Bitfinex has been subjected to several hacking attacks, where vast sums of money were stolen by
                    attackers, putting several things at stake.</p>

                <h3>Bitfinex Hack</h3>
                <p>Here are some things you should know about the hacking that Bitfinex was subjected to.</p>
                <h4>When did the hack occur?</h4>
                <p>The event in question occurred in 2015 when customers of the exchange faced the attack. In this
                    attack, hackers stole Bitcoins worth $400,000. The more significant attack happened in 2016, which
                    witnessed the loss of $73 million from customers. The second attack had a noticeable impact on the
                    trading price of Bitcoin as well.</p>

                <h4>When did they return the money to investors?</h4>
                <p>Something that made people trust Bitfinex is that they managed to return every penny that was stolen.
                    The hack occurred in August 2016, and the paid back all of its investors their dues in September
                    2016. It, too, had an impact on the trading volume and price figures of Bitcoin and USD.</p>
                <h4>How many Bitcoins were stolen?</h4>
                <p>In a major attack, hackers stole 120,000 units of Bitcoin from Bitfinex customers. Back then, the
                    coins were valued at around $72 million. Although Bitfinex reimbursed some of the stolen Bitcoins,
                    most of them are gone with the wind.</p>

                <h4>Licensing</h4>
                <p>Coinbase is a US licensed company, and it has its headquarters in San Francisco. It also means that
                    the firm faced no trouble onboarding customers from the United States. It means that customers from
                    the US can access Coinbase without any trouble.</p>

                <h3>Coinbase vs Bitfinex: Features Comparison</h3>
                <img src="<?php echo get_template_directory_uri() ?>/img/compare-img3.png" alt="">
                <p>Now that we know enough about both Coinbase and Bitfinex, shall we have a look at the features that
                    the firms have to offer? Sometimes, you may have to pick one over the other for the sake of
                    features.</p>

                <h4>Payment Options</h4>
                <h5>Fiat Currencies/Bank Transfer/Cryptocurrency Options</h5>
                <p>Because Coinbase is very beginner-friendly, making payments on the exchange is easier. You can
                    purchase Bitcoins from Coinbase using other cryptocurrencies, credit/debit cards, and direct bank
                    wire transfer. By the way, if you live in the UK, EU, or the US, you can connect your Visa Fast
                    Funds-enabled credit and debit cards for instant card withdrawal as well. Coinbase takes up to 1-5
                    days, 1-2 days, and one day to get the money to the US, EU, and UK customers’ bank accounts,
                    respectively.</p>
            </div>

        </div>
    </div> -->

    <!-- <div class="container">
        <div class="row">
            <div class="col-xl-10 offset-xl-1">
                <div class="crypto-table compare-table">
                    <p class="crypto-title">comparison chart</p>
                    <table id="cryptoTable1" class="table" style="width:100%">
                        <thead>
                            <tr>
                                <th class="empty"></th>
                                <th>Active Users</th>
                                <th>Operating Countries</th>
                                <th>Founded</th>
                                <th>Fiat Currency Trading</th>
                                <th>Payment methods</th>
                                <th>Withdrawal fees</th>
                                <th>Trading fees</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Exchange 1 <a href="">Visit website</a></th>
                                <td>2.1 M</td>
                                <td>EU, USA, Croatia, New Zaeland, Australia, Austria, Germany</td>
                                <td>2018</td>
                                <td><span class="check-icon"></span> EUR, USD</td>
                                <td>Cards, Wiretransfer, Crypto</td>
                                <td>0.2%</td>
                                <td>0.1%</td>
                            </tr>
                            <tr>
                                <th>Exchange 2 <a href="">Visit website</a></th>
                                <td>300,000+</td>
                                <td>Unlimited</td>
                                <td>2018</td>
                                <td><span class="minus-icon"></span></td>
                                <td>Cards, Wiretransfer, Crypto</td>
                                <td>
                                    <div class="tooltip-wrap">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/desktop-icon.png"
                                            alt="">
                                        <span class="tooltip">Desktop app</span>
                                    </div>

                                    <div class="tooltip-wrap">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/mobile-icon.png"
                                            alt="">
                                        <span class="tooltip">Mobile app</span>
                                    </div>
                                </td>
                                <td>0.1%</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-10 offset-xl-1">

                <div class="crypto-table compare-table">
                    <p class="crypto-title">comparison chart</p>
                    <table id="cryptoTable2" class="table" style="width:100%">
                        <thead>
                            <tr>
                                <th class="empty"></th>
                                <th>Price</th>
                                <th>Released</th>
                                <th>Dimensions</th>
                                <th>Screen</th>
                                <th>Coin control</th>
                                <th>Supported coins</th>
                                <th>Software compability</th>
                                <th>Segwit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Wallet name longer 1 <a href="">Visit website</a></th>
                                <td>$12 / mon.</td>
                                <td>2018</td>
                                <td>10x15 cm</td>
                                <td>
                                    <div class="tooltip-wrap">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/desktop-icon.png"
                                            alt="">
                                        <span class="tooltip">Desktop app</span>
                                    </div>

                                    <div class="tooltip-wrap">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/mobile-icon.png"
                                            alt="">
                                        <span class="tooltip">Mobile app</span>
                                    </div>
                                </td>
                                <td><span class="check-icon"></span></td>                              
                                <td>0.2%</td>
                                <td>Windows 10, OSX, iOS</td>
                                <td><span class="check-icon"></span></td>
                            </tr>
                            <tr>
                                <th>Wallet name longer 2 <a href="">Visit website</a></th>
                                <td>Free</td>   
                                <td>2018</td>
                                <td>10x15 cm</td>
                                <td>
                                    <div class="tooltip-wrap">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/desktop-icon.png"
                                            alt="">
                                        <span class="tooltip">Desktop app</span>
                                    </div>

                                    <div class="tooltip-wrap">
                                        <img src="<?php echo get_template_directory_uri() ?>/img/mobile-icon.png"
                                            alt="">
                                        <span class="tooltip">Mobile app</span>
                                    </div>
                                </td>
                                <td><span class="minus-icon"></span></td>   
                                <td>0.2%</td>
                                <td>Windows 10, OSX, iOS</td>
                                <td><span class="minus-icon"></span></td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div> -->

</main>

<?php get_footer(); ?>