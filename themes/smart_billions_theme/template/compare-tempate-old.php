<?php /* Template Name: Compare old*/ get_header(); ?>


<main class="compare">

    <div class="news-head wallets-head">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h1>Compare exchanges compare wallets compare</h1>
                    <p>If you’d like to learn more about crypto, look no more. We’ve created a learning hub that should help you get started with crypto in no time.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">
                <!-- <h1>Compare wallets, exchanges and coins</h1> -->
                <div class="selection-box">
                    <div class="title">
                        <span class="number">1</span>
                        <h3>What would you like to compare?</h3>
                    </div>
                    <ul>
                        <li><a href="">Coins</a></li>
                        <li><a href="">Wallets</a></li>
                        <li><a href="">Exchanges</a></li>

                    </ul>
                </div>
                <div class="all-comparisons">
                    <!-- <h2>All comparisons</h2> -->
                    <div class="comparison">
                        <div class="caption">
                            <h2>Coins</h2>
                            <div class="round-icon">
                                <img src="../wp-content/themes/smart_billions_theme/img/coins-icon.svg"
                                    alt="Coins-icon">
                            </div>
                        </div>
                        <a href="">
                            <h3>Bitcoin Cash vs. Litecoin: Which Cryptocurrency is better in 2021?</h3>
                        </a>
                        <a href="">
                            <h3>Bitcoin Cash vs. Litecoin: Which Cryptocurrency is better in 2021?</h3>
                        </a>
                    </div>


                    <div class="comparison">
                        <div class="caption">
                            <h2>Wallets</h2>
                            <div class="round-icon">
                                <img src="../wp-content/themes/smart_billions_theme/img/wallet-icon.svg"
                                    alt="Wallet-icon">
                            </div>
                        </div>
                        <a href="">
                            <h3>Ledger Nano X vs. S Review: Which Should You Buy?</h3>
                        </a>
                        <a href="">
                            <h3>Ledger Nano X vs. S Review: Which Should You Buy?</h3>
                        </a>

                    </div>


                    <div class="comparison">
                        <div class="caption">
                            <h2>Exchanges </h2>
                            <div class="round-icon">
                                <img src="../wp-content/themes/smart_billions_theme/img/exchange-icon.svg"
                                    alt="Exchange-icon">
                            </div>
                        </div>
                        <a href="">
                            <h3>Coinbase vs Bitstamp: Which is the Better Cryptocurrency Exchange?</h3>
                        </a>
                        <a href="">
                            <h3>Coinbase vs Bittrex: Crypto Exchange Comparison (2021)</h3>
                        </a>
                    </div>



                    <div class="compare-buttons">
                        <button class="button">new comparison</button>
                        <button class="button-neutral">view all</button>
                    </div>
                    <div class="comparison">
                        <div class="caption">
                            <h2>Wallets
                                <span>Mobile</span>
                            </h2>
                            <div class="round-icon">
                                <img src="../wp-content/themes/smart_billions_theme/img/wallet-icon.svg"
                                    alt="Wallet-icon">
                            </div>
                        </div>

                        <a href="">
                            <h3>Ledger Nano X vs. S Review: Which Should You Buy?</h3>
                        </a>
                        <a href="">
                            <h3>Ledger Nano X vs. S Review: Which Should You Buy?</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>