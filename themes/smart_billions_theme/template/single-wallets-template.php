<?php /* Template Name: Single Wallets */ get_header(); ?>

<main class="single-wallets">
    <div class="intro">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-1">
                    <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                    <span class="update update-desktop update-page">Last update:
                        <?php 
                        $u_time = get_the_time('U'); 
                        $u_modified_time = get_the_modified_time('U'); 
                        if ($u_modified_time >= $u_time + 86400) { 
                        the_modified_time('M j, Y'); 
                        } else {
                            echo get_the_date();
                        }
                        ?></span>
                </div>
                <div class="col-xl-8 offset-xl-2">
                    <div class="title">
                        <?php  if(get_field( 'wallet_image' )): ?>

                            <?php $logo_style = get_field( 'logo_style' ); ?>
                            <?php if($logo_style == "black"):?>
                            <?php $logo_style_class = "round-icon--black"; ?>
                            <?php elseif($logo_style == "without"): ?>
                                <?php $logo_style_class = "round-icon--abra" ?>
                                <?php elseif($logo_style == "default"): ?>
                                <?php $logo_style_class = "" ?>
                                <?php else: ?>
                                    <?php $logo_style_class = "" ?>
                            <?php endif; ?>

                            <div class="round-icon <?php echo $logo_style_class; ?>">
                            <?php $wallet_image = get_field( 'wallet_image' ); ?>
                            <?php if ( $wallet_image ) { ?>
                                <img src="<?php echo $wallet_image['url']; ?>" alt="<?php echo $wallet_image['alt']; ?>" />
                            <?php } ?>
                            </div>
                        <?php endif; ?>
                        <h1 class="name"><?php the_title(); ?></h1>
                        <?php if(get_field( 'star_rating' )) {
                            ?>
                        <div class="rating-wrap" style="--rating: <?php the_field( 'star_rating' ); ?>;">
                            <div class="rating"></div>
                            <small><?php the_field( 'star_rating' ); ?>/5</small>
                        </div>
                        <?php
                        } ?>
                    </div>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">
                <ul class="info">
                    <li>
                        <div class="title-box">
                            <h4><?php the_field( 'global_wallet_name' ); ?></h4>
                        </div>
                        <a target="_blank" class="brand-link"
                            href="<?php the_field( 'official_site_url' ); ?>"><?php echo parse_url(get_field( 'official_site_url' ), PHP_URL_HOST); ?></a>
                        <?php if ( have_rows( 'additional_info' ) ) : ?>
                        <ol>
                            <?php while ( have_rows( 'additional_info' ) ) : the_row(); ?>
                            <li>
                                <small><?php the_sub_field( 'label' ); ?></small> <?php the_sub_field( 'info' ); ?>
                            </li>
                            <?php endwhile; ?>
                        </ol>
                        <?php endif; ?>
                        <?php if ( get_field( 'add_nofollow_link' ) == 1 ) { 
                            $nofollow = "rel='nofollow'";
                        } else { 
                            $nofollow = "";
                        } ?>

                        <?php if(get_field( 'button_name' )): ?>
                            <div class="buttons">
                                <a target="_blank" <?php echo $nofollow; ?> href="<?php the_field( 'official_site_url' ); ?>" class="button"><?php the_field( 'button_name' ); ?></a>
                            </div>
                        <?php else: ?>
                            <div class="buttons">
                                <a target="_blank" <?php echo $nofollow; ?> href="<?php the_field( 'official_site_url' ); ?>" class="button">Visit site</a>
                            </div>
                        <?php endif; ?>
                       
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Include pros and cons section  -->
    <?php get_template_part( 'template-parts/components/pros', 'cons' ); ?>
    <!-- End pros and cons section  -->

    <!-- Include flexible single content  -->
    <?php get_template_part( 'template-parts/flexible-single', 'leftalign' ); ?>
    <!-- End flexible single content  -->

    <!-- Author section  -->
   <?php get_template_part( 'template-parts/author', 'section' ); ?>
   <!-- End author section  -->
                       

    <!-------------------------------- SICKY FOOTER -->

    <div class="info-footer">
        <div class="container">
            <?php $logo_style = get_field( 'logo_style' ); ?>
            <?php if($logo_style == "black"):?>
            <?php $logo_style_class = "round-icon--black"; ?>
            <?php elseif($logo_style == "without"): ?>
                <?php $logo_style_class = "round-icon--abra" ?>
                <?php elseif($logo_style == "default"): ?>
                <?php $logo_style_class = "" ?>
                <?php else: ?>
                <?php $logo_style_class = "" ?>
            <?php endif; ?>
            <?php $wallet_image = get_field( 'wallet_image' ); ?>

            <?php if ( $wallet_image ) { ?>
            <div class="round-icon <?php echo $logo_style_class; ?>">
                <img src="<?php echo $wallet_image['url']; ?>" alt="<?php echo $wallet_image['alt']; ?>" />
            </div>
            <?php } ?>
            <div class="title-wrap">
                <?php if(get_field( 'global_wallet_name' )): ?>
                    <h4><?php the_field( 'global_wallet_name' ); ?></h4>
                <?php endif; ?>
                <?php if(get_field( 'star_rating' )): ?>
                <div class="rating-wrap" style="--rating: <?php the_field( 'star_rating' ); ?>;">
                    <div class="rating"></div>
                    <small><?php the_field( 'star_rating' ); ?>/5</small>
                </div>
                <?php endif; ?>
            </div>

            <?php if ( get_field( 'add_nofollow_link' ) == 1 ) { 
                $nofollow = "rel='nofollow'";
            } else { 
                $nofollow = "";
            } ?>

            <?php if(get_field( 'button_name' )): ?>
                <a target="_blank" <?php echo $nofollow; ?> href="<?php the_field( 'official_site_url' ); ?>" class="button"><?php the_field( 'button_name' ); ?></a>
            <?php else: ?>
                <a target="_blank" <?php echo $nofollow; ?> href="<?php the_field( 'official_site_url' ); ?>" class="button">Visit site</a>
            <?php endif; ?>
        </div>
    </div>

</main>
<?php get_footer(); ?>