<?php /* Template Name: Single country */ get_header(); ?>
<main class="single-country">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">
                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                <span class="update update-desktop update-page">Last update:
                <?php 
                $u_time = get_the_time('U'); 
                $u_modified_time = get_the_modified_time('U'); 
                if ($u_modified_time >= $u_time + 86400) { 
                the_modified_time('M j, Y'); 
                } else {
                    echo get_the_date();
                }
                ?></span>
            </div>
            <div class="col-xl-8 offset-xl-2">
                <div class="title">
                    <div class="round-coin-icon">
                        <?php $country_image = get_field( 'country_image' ); ?>
                        <?php if ( $country_image ) { ?>
                            <img src="<?php echo $country_image['url']; ?>" alt="<?php echo $country_image['alt']; ?>" />
                        <?php } ?>
                    </div>
                    <h1><?php the_title(); ?></h1>
                </div>
                <div class="text-intro"><?php the_content(); ?></div>
            </div>
        </div>
    </div>

    <!-- Include flexible single content  -->
    <?php get_template_part( 'template-parts/flexible-single', 'leftalign' ); ?>
    <!-- End flexible single content  -->

    <!-- Author section  -->
    <?php //get_template_part( 'template-parts/author', 'section' ); ?>
    <!-- End author section  -->
       
</main>
<?php get_footer(); ?>