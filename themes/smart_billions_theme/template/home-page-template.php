<?php /* Template Name: Home page */ get_header(); ?>

<main>

    <div class="home-hero">
        <div class="container">
            <div class="home-hero__wrap">
                <h1>All your crypto information in one place. SmartBillions.com</h1>
                <p>Get exclusive content updates, hot tips and tricks for investing and special offers from our
                    partners on
                    e-mail.</p>

                <?php echo do_shortcode("[activecampaign form=2 css=0]");?>
            </div>
        </div>
    </div>

    <div class="home-explore">
        <div class="container">
            <div class="home-explore__head">
                <h2>Latest, regularly updated and trustworthy cryptocurrency news, knowedge, comparisons, guides and
                    more..
                </h2>
                <a href="" class="button">Explore</a>
            </div>
            <div class="row">
                <div class="col-sm-6 col-xl-4">
                    <div class="explore-item">
                        <a href="">
                            <div class="round-icon">
                                <img src="../wp-content/themes/smart_billions_theme/img/news-icon.svg"
                                    alt="Wallet-icon">
                            </div>
                        </a>
                        <div class="explore-content">
                            <a href="">
                                <h3>News</h3>
                            </a>
                            <p>Get informed on latest <a href="">cryptocurrency news</a> from verified sources.</p>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="explore-item">
                        <a href="">
                            <div class="round-icon">
                                <img src="../wp-content/themes/smart_billions_theme/img/buying-guides.svg"
                                    alt="Wallet-icon">
                            </div>
                        </a>
                        <div class="explore-content">
                            <a href="">
                                <h3>Learn</h3>
                            </a>
                            <p>Learn all about cryptocurrencies, wallets and exchanges from our comprehensive <a
                                    href="">knowledgebase</a>.</p>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="explore-item">
                        <a href="">
                            <div class="round-icon">
                                <img src="../wp-content/themes/smart_billions_theme/img/comparisons-icon.svg"
                                    alt="Comparisons-icon">
                            </div>
                        </a>
                        <div class="explore-content">
                            <a href="">
                                <h3>Comparisons & Reviews</h3>
                            </a>
                            <p>Trustworthy, handmade <a href="">comparisons</a> of cryptocurrencies, wallets and
                                exchanges. </p>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="explore-item">
                        <a href="">
                            <div class="round-icon">
                                <img src="../wp-content/themes/smart_billions_theme/img/coin-icon.png" alt="Coin-icon">
                            </div>
                        </a>
                        <div class="explore-content">
                            <a href="">
                                <h3>Coin guides</h3>
                            </a>
                            <p>Comprehensive <a href="">coin buying guides</a> to help you make a proper decision.</p>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-4">
                    <div class="explore-item">
                        <a href="#">
                            <div class="round-icon">
                                <img src="../wp-content/themes/smart_billions_theme/img/flag-united-kingdom.svg"
                                    alt="Comparisons-icon">
                            </div>
                        </a>
                        <div class="explore-content">
                            <a href="#">
                                <h3>Country guides</h3>
                            </a>
                            <p>Informative and regularly updated <a href="">country guides</a> for major markets.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="home-recently">
        <div class="container">
            <h2>Recently published</h2>
            <div class="row">

                <div class="col-md-4">
                    <div class="global-item">
                        <span class="update update-desktop">Last update: Nov 20, 2020</span>
                        <a href="">
                        <h3>Coinbase vs. Bitstamp: Which is the Better Cryptocurrency Exchange?</h3>
                       </a>
                        <p>Alongside Kraken, Bittrex and Binance, Coinbase and Bitstamp are some of the most popular
                            crypto exchanges out there.</p>
                        <a href="" class="button">Read more</a>
                        <div class="update-mobile">
                            <span class="update">Last update:</span>
                            <span class="update">Nov 20, 2020</span>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="global-item">
                        <span class="update update-desktop">Last update: Nov 20, 2020</span>
                        <h3>Coinbase vs Bittrex: Crypto Exchange Comparison (2021)</h3>
                        <p>In the ever-evolving crypto space, we have to stay vigilant. Most crypto exchanges and
                            trading platforms are working around the clock in bettering their services.</p>
                        <a href="" class="button">Read more</a>
                        <div class="update-mobile">
                            <span class="update">Last update:</span>
                            <span class="update">Nov 20, 2020</span>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="global-item">
                        <span class="update update-desktop">Last update: Nov 20, 2020</span>
                        <h3>Coinbase vs. Bitstamp: Which is the Better Cryptocurrency Exchange?</h3>
                        <p>Alongside Kraken, Bittrex and Binance, Coinbase and Bitstamp are some of the most popular
                            crypto
                            exchanges out there.</p>
                        <a href="" class="button">Read more</a>
                        <div class="update-mobile">
                            <span class="update">Last update:</span>
                            <span class="update">Nov 20, 2020</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


</main>
<?php get_footer(); ?>