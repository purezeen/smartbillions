<?php /* Template Name: Buy coins */ get_header(); ?>

 <div class="page-list">
   <div class="container">
      <div class="page-list__head">
         <div class="row">
            <div class="col-12 offset-xl-1">
               <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
            </div>
            <div class="col-lg-6 col-xl-4 offset-xl-1">
               <h1><?php the_field( 'title' ); ?></h1>
            </div>
            <div class="col-lg-6 col-xl-4">
               <?php the_content(); ?>
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-xl-9 offset-xl-1">
            <h2>Coin guides</h2>

            <div class="coins-wrap">
               <?php
               $parent_id = get_the_ID();
               $args = array(
                     'post_parent' => $parent_id, 
                     'post_type'   => 'page',
                     'posts_per_page' => -1,
                     'post_status' => 'publish'
                     );
               $child_pages_query = new WP_Query($args);
               ?>

               <?php if ($child_pages_query->have_posts()) : ?>
                     <?php while ($child_pages_query->have_posts()) : $child_pages_query->the_post(); ?>
                        <a href="<?php the_permalink(); ?>" class="coin">
                           <?php $currency_image = get_field( 'currency_image' ); ?>
                           <?php $country_image = get_field( 'country_image' ); ?>
                           <?php if ( $currency_image ) { ?>
                                 <div class="round-coin-icon">
                                    <img src="<?php echo $currency_image['url']; ?>" alt="<?php echo $currency_image['alt']; ?>" />
                                 </div>
                           <?php } elseif ($country_image) { ?>
                                 <div class="round-coin-icon">
                                    <img src="<?php echo $country_image['url']; ?>" alt="<?php echo $country_image['alt']; ?>" />
                                 </div>
                           <?php } ?>
                           <?php if ( get_field( 'global_crypto_name' ) && get_field( 'currency' ) ): ?>
                                 <div>
                                    <h4><?php the_field( 'global_crypto_name' ) ?></h4><span><?php the_field( 'currency' ) ?></span>
                                 </div>
                           <?php elseif ( get_field( 'global_country_name' ) ) : ?>
                                 <div>
                                    <h4><?php the_field( 'global_country_name' ) ?></h4>
                                 </div>
                           <?php else: ?>
                                 <div>
                                    <h4><?php the_title(); ?></h4>
                                 </div>
                           <?php endif; ?>
                           <button class="round-arrow-right"></button>
                        </a>
                     <?php endwhile; ?>
                     <?php wp_reset_postdata(); ?>
                     <?php wp_reset_query(); ?>
               <?php endif; ?>
            <?php //endif; ?>
            <!-- END Check If post object have multiple value -->

            </div>
         </div>
      </div>

   </div>
</div>

<?php get_footer(); ?>