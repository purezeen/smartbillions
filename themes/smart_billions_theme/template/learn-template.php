<?php /* Template Name: Learn */ get_header(); ?>

<main class="learn">
   <div class="news-head wallets-head">
      <div class="container">
         <div class="row">
            <div class="col-lg-6">
                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                <?php the_content(); ?>
            </div>
         </div>
      </div>
   </div>
   <div class="learn-content">

   <?php 
    // Get Learn page ID by his ancestor
    // $childrens = get_pages( array( 'child_of' => get_the_ID() ) );
    // foreach ($childrens as $children) {
     // if ($children->post_name == 'learn') {
         // $post_parent = $children->ID;
     // }
    // }
    $guide_list = get_field( 'guide_list' );
    $args = array(
        'post_parent' => $guide_list,
        'child_of' => get_the_ID(),
        'post_type'   => 'page',
        'posts_per_page' => -1,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post_status' => 'publish' ,
        'category_name' => 'guide+learn',
        );
    $guide_query = new WP_Query($args);
    ?>

    <?php if ($guide_query->have_posts()) : ?>
    <div class="crypto-head crypto-slider--learn">
        <div class="container">
            <h2><?php the_field( 'section_title_for_guides' ); ?></h2>
            <div class="carousel">
            <?php $i = 1; ?>
            <?php while ($guide_query->have_posts()) : $guide_query->the_post(); ?>
                <a href="<?php the_permalink(); ?>" class="item"
                    style="background-image:url(<?php echo get_template_directory_uri() ?>/img/slider-mask<?php echo $i; ?>.svg)">
                    <button class="button-round-white">Part <?php the_field( 'number_of_part' ); ?></button>
                    <h3><?php the_title(); ?></h3>
                </a>
                <?php $i++; ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
<!-- slider end -->

<?php
$page_categories = get_the_category();
$root_category = $page_categories[0]->cat_ID;
$subcategories =  get_categories('child_of='.$root_category);  
?>

<?php $choose_the_category_ids = get_field( 'choose_the_category' ); ?>
  
      <div class="container">
        <?php $parent_id = get_the_ID();?>
        <?php if($choose_the_category_ids) { ?>
            <?php foreach ($choose_the_category_ids as $cat) {
            $cat_name = get_cat_name( $cat);
            $args = array(
                // 'post_parent' => $parent_id,
                'post_type'   => 'page',
                'posts_per_page' => -1,
                'post_status' => 'publish',
                'orderby'    => 'modified',
                'category__in' => array($cat),
                );
                $wp_query = new WP_Query($args); ?>

                <?php if ($wp_query->have_posts()) : ?>

            <?php
            // Define taxonomy prefix
            // Replace NULL with the name of the taxonomy eg 'category'
            $taxonomy_prefix = 'category';

            // Define term ID
            // Replace NULL with ID of term to be queried eg '123'
            $term_id = $cat;

            // Define prefixed term ID
            $term_id_prefixed = $taxonomy_prefix .'_'. $term_id; ?>

                <?php if (have_rows('category', $term_id_prefixed)) : ?>
                    <?php while (have_rows('category', $term_id_prefixed)) : the_row(); ?>
                        <h2 id="<?php if($cat_name) { echo strtolower($cat_name); }?>"><?php the_sub_field('title'); ?></h2>
                    <?php endwhile; ?>
                <?php endif; ?>
                <div class="row">
                    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                    <div class="col-md-6 col-lg-4">
                    <div class="global-item">
                        <div><span class="update update-desktop">Last update:
                        <?php 
                        $u_time = get_the_time('U'); 
                        $u_modified_time = get_the_modified_time('U'); 
                        if ($u_modified_time >= $u_time + 86400) { 
                        the_modified_time('M j, Y'); 
                         } else {
                            echo get_the_date();
                         }
                        ?>
                        </span>
                        </div>
                        <a href="<?php the_permalink(); ?>">
                            <h3><?php the_title(); ?></h3>
                        </a>
                        <?php the_excerpt(); ?>

                        <a href="<?php the_permalink(); ?>" class="button">Read more</a>
                        <div class="update-mobile">
                            <span class="update">Last update:</span>
                            <span class="update"> 
                            <?php 
                            $u_time = get_the_time('U'); 
                            $u_modified_time = get_the_modified_time('U'); 
                            if ($u_modified_time >= $u_time + 86400) { 
                            the_modified_time('M j, Y'); 
                            } else {
                                echo get_the_date();
                             }
                        ?></span>
                        </div>

                    </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
                <?php
            }
        }?>
      </div>
   </div>
</main>

<?php get_footer(); ?>