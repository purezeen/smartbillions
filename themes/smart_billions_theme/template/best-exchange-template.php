<?php /* Template Name: Best Exchange */ get_header(); ?>

<main class="best-exchange">
    <div class="news-head wallets-head">
        <div class="container editor">
            <div class="row">
                <div class="col-xl-8 offset-xl-1">
                    <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                    <span class="update update-desktop update-page">Last update:
                    <?php 
                    $u_time = get_the_time('U'); 
                    $u_modified_time = get_the_modified_time('U'); 
                    if ($u_modified_time >= $u_time + 86400) { 
                    the_modified_time('M j, Y'); 
                    } else {
                        echo get_the_date();
                    }
                    ?></span>
                </div>
                <div class="col-xl-6 offset-xl-2">
                    <h1><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Include flexible single content  -->
    <?php get_template_part( 'template-parts/flexible-single', 'leftalign' ); ?>
    <!-- End flexible single content  -->    

    <!-- Author section  -->
    <?php get_template_part( 'template-parts/author', 'section' ); ?>
    <!-- End author section  -->
    
</main>
<?php get_footer(); ?>