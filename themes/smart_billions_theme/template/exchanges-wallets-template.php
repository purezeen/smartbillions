<?php /* Template Name: Exchanges and Wallets */ get_header(); ?>


<?php
$page_categories = get_the_category();

if($page_categories) {
    $root_category = $page_categories[0]->cat_ID;
    $subcategories =  get_categories('child_of='.$root_category);  
}
?>

<?php $choose_the_category_ids = get_field( 'choose_the_category' ); ?>
<main class="exchanges wallets">
    <div class="news-head wallets-head">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>

                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="container">
        <?php $parent_id = get_the_ID(); ?>
        <?php if($choose_the_category_ids) { ?>
            <?php foreach ($choose_the_category_ids as $cat) {
                $cat_name = get_cat_name( $cat);
                $args = array(
                    'post_parent' => $parent_id,
                    'post_type'   => 'page',
                    'posts_per_page' => -1,
                    'post_status' => 'publish' ,
                    'orderby'    => 'modified',
                    'category__in' => array($cat),
                    );
                $wp_query = new WP_Query($args); ?>

                <?php if ($wp_query->have_posts()) : ?>

                    <?php
                    // Define taxonomy prefix
                    // Replace NULL with the name of the taxonomy eg 'category'
                    $taxonomy_prefix = 'category';

                    // Define term ID
                    // Replace NULL with ID of term to be queried eg '123'
                    $term_id = $cat;

                    // Define prefixed term ID
                    $term_id_prefixed = $taxonomy_prefix .'_'. $term_id; ?>

                    <?php if (have_rows('category', $term_id_prefixed)) : ?>
                        <?php while (have_rows('category', $term_id_prefixed)) : the_row(); ?>
                            <div class="section-header" id="<?php if($cat_name) { echo strtolower($cat_name); }?>">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-6">
                                        <div class="title">
                                        <?php $icon = get_sub_field('icon'); ?>
                                        <?php if ($icon) { ?>
                                            <div class="round-icon">
                                                <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
                                            </div>
                                        <?php } ?>                 
                                            <h2><?php the_sub_field('title'); ?></h2>
                                        </div>
                                    </div>
                                    <div class="col-xl-5 col-lg-6">
                                        <p><?php the_sub_field('description'); ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>

                    <div class="row items-wrap">
                        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                        <div class="col-md-6 col-lg-4">
                            <div class="global-item">
                                <div><span class="update update-desktop">Last update:
                                <?php 
                                    $u_time = get_the_time('U'); 
                                    $u_modified_time = get_the_modified_time('U'); 
                                    if ($u_modified_time >= $u_time + 86400) { 
                                    the_modified_time('M j, Y'); 
                                    } else {
                                        echo get_the_date();
                                     }
                                    ?></span>
                                </div>
                                <a href="<?php the_permalink(); ?>">
                                    <h3><?php the_title(); ?></h3>
                                </a>
                                <?php the_excerpt(); ?>

                                <a href="<?php the_permalink(); ?>" class="button">Read more</a>
                                <div class="update-mobile">
                                    <span class="update">Last update:</span>
                                    <span class="update"> <?php 
                                    $u_time = get_the_time('U'); 
                                    $u_modified_time = get_the_modified_time('U'); 
                                    if ($u_modified_time >= $u_time + 86400) { 
                                    the_modified_time('M j, Y'); 
                                    } else {
                                        echo get_the_date();
                                     }
                                    ?></span>
                                </div>

                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>
                <?php
            } ?>
            <?php } ?>
      </div>
   </div>
</main>

<?php get_footer(); ?>