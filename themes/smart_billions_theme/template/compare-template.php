<?php /* Template Name: Compare */ get_header(); ?>


<div class="news-head wallets-head">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</div>

<main class="compare">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">
                <!-- <?php if ( have_rows( 'choose_the_categories' ) ) : ?>
                    <h1 class="compare-title">Compare 
                        <?php while ( have_rows( 'choose_the_categories' ) ) : the_row(); ?>     
                        <?php $choose_the_parent_category_terms = get_sub_field( 'choose_the_parent_category' ); ?>
                        <?php if ( $choose_the_parent_category_terms ): ?>
                            <?php foreach ( $choose_the_parent_category_terms as $choose_the_parent_category_term ): ?>
                            <span><?php echo $parent_cat_id =  $choose_the_parent_category_term->name; ?></span>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php endwhile; ?>
                    </h1>
                <?php endif; ?>
                <div class="selection-box">
                    <div class="parent_wrap" >
                    <div class="title">
                        <span class="number">1</span>
                        <h3><?php the_field( 'first_question' ); ?></h3>
                    </div>
                        <ul>
                            <?php if ( have_rows( 'choose_the_categories' ) ) : ?>
                                <?php while ( have_rows( 'choose_the_categories' ) ) : the_row(); ?>
                                    <?php $choose_the_parent_category_terms = get_sub_field( 'choose_the_parent_category' ); ?>
                                    <?php if ( $choose_the_parent_category_terms ): ?>
                                        <?php foreach ( $choose_the_parent_category_terms as $choose_the_parent_category_term ): ?>
                                            <?php if ( get_sub_field( 'has_child_categories' ) == 1 ) { 
                                            ?>
                                            <?php $parent_id = $choose_the_parent_category_term->term_id; ?>
                                                <li><a class="parent_item" href="#" data-cat="<?php echo $parent_id; ?>"><?php echo $choose_the_parent_category_term->name; ?></a></li>
                                            <?php
                                                } else { 
                                                ?>
                                                <?php $parent_cat_id = $choose_the_parent_category_term->term_id; ?>
                                                <?php $parent_cat_url = get_category_link($parent_cat_id ); ?>
                                                <li class="js-filter-item"><a class="parent_only" data-category="<?php echo $parent_cat_id; ?>"  data-child="true" href="<?php echo $parent_cat_url; ?>"><?php echo $choose_the_parent_category_term->name; ?></a></li>
                                                <?php
                                                } ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <?php if ( have_rows( 'choose_the_categories' ) ) : ?>
                        <?php while ( have_rows( 'choose_the_categories' ) ) : the_row(); ?>     
                        
                        <?php $choose_the_parent_category_terms = get_sub_field( 'choose_the_parent_category' ); ?>
                        <?php if ( $choose_the_parent_category_terms ): ?>
                            <?php foreach ( $choose_the_parent_category_terms as $choose_the_parent_category_term ): ?>
                                <?php $parent_cat_id =  $choose_the_parent_category_term->term_id; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>

                            <?php $choose_the_child_category_terms = get_sub_field( 'choose_the_child_category' ); ?>
                            <?php if ( $choose_the_child_category_terms ): ?>
                                <div class="child-wrap" data-cat="<?php echo $parent_cat_id;?>">
                                    <div class="title">
                                        <span class="number">2</span>
                                        <h3><?php the_sub_field( 'question_for_child_categories' ); ?></h3>
                                    </div>
                                    <ul>
                                        <?php foreach ( $choose_the_child_category_terms as $choose_the_child_category_term ): ?>                            
                                            <?php $cat_id = $choose_the_child_category_term->term_id; ?>
                                            <?php $cat_url = get_category_link($cat_id); ?>
                                            <?php $cat_name = $choose_the_child_category_term->name; ?>
                                        <li class="js-filter-item"><a data-category="<?php echo $cat_id; ?>" href="<?php echo $cat_url; ?>"><?php echo $cat_name; ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div> -->
              
                <div class="js-filter">
                    <div class="all-comparisons">
                    <h2>All comparisons</h2>                      
                    <?php if ( have_rows( 'choose_the_categories' ) ) : ?>
                        <?php while ( have_rows( 'choose_the_categories' ) ) : the_row(); ?>

                            <?php $parent_id = array(); ?>
                            <?php $choose_the_parent_category_terms = get_sub_field( 'choose_the_parent_category' ); ?>
                            <?php if ( $choose_the_parent_category_terms ): ?>
                                <?php foreach ( $choose_the_parent_category_terms as $choose_the_parent_category_term ): ?>
                                    <?php $parent_id[] =  $choose_the_parent_category_term->term_id; ?>
                                    <?php $parent_name =  $choose_the_parent_category_term->name; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php $child_id = array(); ?>
                            <?php $choose_the_child_category_terms = get_sub_field( 'choose_the_child_category' ); ?>
                            <?php if ( $choose_the_child_category_terms ): ?>
                                <?php foreach ( $choose_the_child_category_terms as $choose_the_child_category_term ): ?>
                                    <?php $child_id[] = $choose_the_child_category_term->term_id; ?>
                                    <?php $cat_name =  $choose_the_child_category_term->name; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <div class="comparison">
                                <div class="caption">
                                    <h2><?php echo $parent_name; ?></span>
                                    </h2>

                                    <?php
                                    // Define taxonomy prefix
                                    // Replace NULL with the name of the taxonomy eg 'category'
                                    $taxonomy_prefix = 'category';

                                    // Define term ID
                                    // Replace NULL with ID of term to be queried eg '123'
                                    $term_id = $choose_the_parent_category_term->term_id;

                                    // Define prefixed term ID
                                    $term_id_prefixed = $taxonomy_prefix .'_'. $term_id; ?>

                                    <?php if ( have_rows( 'category', $term_id_prefixed ) ) : ?>
                                        <?php while ( have_rows( 'category', $term_id_prefixed ) ) : the_row(); ?>
                                            <?php $icon = get_sub_field( 'icon', $term_id_prefixed ); ?>
                                            <?php if ( $icon ) { ?>
                                                <div class="round-icon">
                                                    <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
                                                </div>
                                            <?php } ?>
                                           
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                </div>
                                <?php
                                $args = array(
                                // 'post_parent' => $parent_id,
                                'post_type'   => 'page',
                                'posts_per_page' => -1,                               );

                                if(empty($child_id)){
                                    $args['category__in'] = $parent_id;
                                }else {
                                    $args['category__in'] = $child_id;
                                }
                            
                                $wp_query = new WP_Query($args); ?>
                                
                                <?php if ($wp_query->have_posts()) : ?>

                                    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_title('<h3>','</h3>') ?>
                                        </a>
                                        
                                    <?php endwhile; ?>
                                    <?php wp_reset_query(); ?>
                                <?php endif; ?>
                            
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>