<?php /* Template Name: Single learn */ get_header(); ?>

<main class="single-learn">
   <div class="intro">
      <div class="news-head">
         <div class="container">
            <div class="row">
               <div class="col-xl-5 offset-xl-2">
                  <!-- <?php $categories = get_the_category(); ?>
                  <a>
                     <?php $category_list = join(', ', wp_list_pluck($categories, 'name')); ?>
                     <?php echo $category_list; ?>
                  </a> -->
                  <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                  <span class="update update-desktop update-page">Last update:
						<?php 
						$u_time = get_the_time('U'); 
						$u_modified_time = get_the_modified_time('U'); 
						if ($u_modified_time >= $u_time + 86400) { 
						the_modified_time('M j, Y'); 
						} else {
							echo get_the_date();
						}
						?></span>
                  <h1><?php the_title(); ?></h1>
               </div>
            </div>
         </div>
      </div>
      <div class="container">
         <div class="row">
            <div class="col-xl-8 offset-xl-2">
               <?php the_content(); ?>
            </div>
         </div>
      </div>
   </div>

   <!-- Include flexible single content  -->
   <?php get_template_part( 'template-parts/flexible-single', 'centeralign' ); ?>
   <!-- End flexible single content  -->

   <!-- Author section  -->
   <?php get_template_part( 'template-parts/author', 'section' ); ?>
   <!-- End author section  -->

   <!-- Next articles  -->
   <?php get_template_part( 'template-parts/next', 'articles' ); ?>
   <!-- End next articles  -->

</main>
<?php get_footer(); ?>