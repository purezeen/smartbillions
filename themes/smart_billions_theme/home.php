<?php get_header(); ?>
<?php 
   // $currCat = get_category(get_query_var('cat'));
   // $cat_name = $currCat->name;
   // $cat_id   = get_cat_ID( $cat_name );
?>

<main class="news">
    <div class="news-head">
        <div class="container">
            <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
            <h1>Cryptocurrency News</h1>
        </div>
    </div>
   <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
   <?php $wp_query = null; ?>
   <?php $temp = $wp_query; ?>
  
   <?php $wp_query = new WP_Query(); ?>
   <?php $args = array(
      'post_type' => 'post',
      'paged' => $paged,
      'showposts' => 6,
      'post_status' => 'publish'
   ); ?>

   <?php $wp_query->query($args); ?>
    
   <?php if ( $wp_query->have_posts() ) : ?>

      <div class="news-items">
         <div class="container">
            <div class="row">

               <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                  <div class="col-md-4">
                     <div class="global-item">
                        <span class="update update-desktop">Last update:
                           <?php 
                           $u_time = get_the_time('U'); 
                           $u_modified_time = get_the_modified_time('U'); 
                           if ($u_modified_time >= $u_time + 86400) { 
                           the_modified_time('M j, Y'); 
                           } else {
                              echo get_the_date();
                           }
                           ?>
                        </span>
                        <a href="<?php echo get_permalink(); ?>">
                           <h3><?php the_title(); ?></h3>
                        </a>
                        <a href="<?php echo get_permalink(); ?>" class="button">Read more</a>
                        <div class="update-mobile">
                           <span class="update">Last update:
                              <?php 
                              $u_time = get_the_time('U'); 
                              $u_modified_time = get_the_modified_time('U'); 
                              if ($u_modified_time >= $u_time + 86400) { 
                              the_modified_time('M j, Y'); 
                              } else {
                                 echo get_the_date();
                              }
                              ?></span>
                        </div>
                     </div>
                  </div>
               <?php endwhile; ?>
               <?php 
                  wp_reset_postdata();;
               ?>
            </div>
           <?php

            ?>
            <!-- pagination -->
            <div class="pagination">
              <?php html5wp_pagination(); ?>
            </div>
            <!-- /pagination -->
         </div>
      </div>

   <?php endif; ?>
</main>

<?php get_footer(); ?>