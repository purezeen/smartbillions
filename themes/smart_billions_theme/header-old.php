<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
    <link rel="apple-touch-icon" sizes="180x180" href="http://blank.local/wp-content/themes/smart_billions_theme/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="http://blank.local/wp-content/themes/smart_billions_theme/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="http://blank.local/wp-content/themes/smart_billions_theme/favicon-16x16.png">
    <link rel="manifest" href="http://blank.local/wp-content/themes/smart_billions_theme/site.webmanifest">
</head>

<body <?php body_class(); ?>>
    <div id="page" class="site">
        <header id="masthead" class="header" role="banner">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-6 col-xl-2">
                        <div class="logo">
                            <a href="/" class="site-branding">
                                <img src="<?php echo get_template_directory_uri() ?>/img/Logo.svg" alt="Logo">
                            </a>
                            <span class="logo-btn">Crypto</span>
                            <div class="logo-menu">
                                 <a class="sub-menu" href="#">
                                     <img src="<?php echo get_template_directory_uri() ?>/img/Logo.svg" alt="Logo">
                                     <span>Crypto</span>
                                 </a>
                            </div>
                        </div>
                        <!-- logo -->
                    </div>
                    <?php get_template_part( 'searchform' ); ?>

                    <div class="col-12 col-xl-8 order-xl-1">
                        <nav class="nav scroll-pane">

                            <?php 

                            wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => 'navigation-menu', 'container' => '' ) ); 

                            // if (is_page()) {
                            //     $id = get_the_ID();
                            //     if (count($ancestors = get_ancestors($id, 'page')))
                            //         $id = $ancestors[count($ancestors)-1];
                            //     wp_nav_menu(get_post($id)->post_name.'-menu');
                            // } else
                            //     wp_nav_menu();

                            ?> 
                        </nav><!-- site navigation -->
                    </div>
                    <?php 
                    $menu_name = 'primary';
                    $locations = get_nav_menu_locations();
                    $menu_id   = $locations[ $menu_name ] ;
                    $menu_items = wp_get_nav_menu_items($menu_id);
                    foreach ($menu_items as $menu_item) { ?>
                        <?php if (get_field( 'enable_mega_menu', $menu_item->ID ) == 1): ?>
                            <div class="dropdown" id="dropdown-<?php the_field( 'mega_menu_toggle_class', $menu_item->ID ); ?>-content">
                                <div class="container">
                                    <div class="modal-header">
                                        <span><?php echo $menu_item->post_title; ?></span>
                                        <span class="close"></span>
                                    </div>

                                    <div class="dropdown-head">
                                        <div class="row">
                                            <div class="col-lg-6 col-xl-4 offset-xl-1">
                                                <h1><?php the_field('mega_menu_title', $menu_item->ID); ?></h1>
                                            </div>
                                            <div class="col-lg-6 col-xl-4">
                                                <p><?php the_field( 'mega_menu_description', $menu_item->ID ); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $post_object = get_field( 'child_pages_as_list', $menu_item->ID ); ?>
                                    <?php $count_post_object = count($post_object); ?>

                                    <?php if ( $post_object ): ?>
                                    <div class="row">
                                        <div class="col-xl-9 offset-xl-1">
                                            <h2><?php the_field( 'mega_menu_list_title', $menu_item->ID ); ?></h2>

                                            <div class="coins-wrap">
                                            <!-- If post object have multiple value -->
                                            <?php if ($count_post_object > 1) : ?>
                                                
                                                    <?php foreach( $post_object as $post_obj ): ?>
                                                       <div class="coin">
                                                        <?php 
                                                            $permalink = get_permalink( $post_obj->ID );
                                                            $title = get_the_title( $post_obj->ID );
                                                            $currency_image = get_field( 'currency_image', $post_obj->ID );
                                                            $country_image = get_field( 'country_image', $post_obj->ID );
                                                         ?>
                                                        <?php if ( $currency_image ) { ?>
                                                            <div class="round-coin-icon">
                                                                <img src="<?php echo $currency_image['url']; ?>" alt="<?php echo $currency_image['alt']; ?>" />
                                                            </div>
                                                        <?php } elseif ($country_image) { ?>
                                                            <div class="round-coin-icon">
                                                                <img src="<?php echo $country_image['url']; ?>" alt="<?php echo $country_image['alt']; ?>" />
                                                            </div>
                                                        <?php } ?>
                                                        <?php if ( get_field( 'global_crypto_name', $post_obj->ID ) && get_field( 'currency', $post_obj->ID ) ): ?>
                                                            <div>
                                                                <h4><?php the_field( 'global_crypto_name', $post_obj->ID ) ?></h4><span><?php the_field( 'currency', $post_obj->ID ) ?></span>
                                                            </div>
                                                        <?php elseif ( get_field( 'global_country_name', $post_obj->ID ) ) : ?>
                                                            <div>
                                                                <h4><?php the_field( 'global_country_name', $post_obj->ID ) ?></h4>
                                                            </div>
                                                        <?php else: ?>
                                                            <div>
                                                                <h4><?php echo esc_html( $title ); ?></h4>
                                                            </div>
                                                        <?php endif ?>
                                                        <a href="<?php echo esc_url( $permalink ); ?>" class="round-arrow-right"></a>
                                                    </div>
                                                    <?php endforeach; ?>
                                                

                                            <!-- If post object don't have multiple value -->
                                            <?php else : ?>
                                                <?php $post = $post_object;  ?>
                                                <?php setup_postdata( $post ); 
                                                $args = array(
                                                    'post_parent' => $post[0]->ID,
                                                    'post_type'   => 'page',
                                                    'posts_per_page' => -1,
                                                    'post_status' => 'publish'
                                                    );
                                                $child_pages_query = new WP_Query($args);
                                                ?>

                                                <?php if ($child_pages_query->have_posts()) : ?>
                                                    <?php while ($child_pages_query->have_posts()) : $child_pages_query->the_post(); ?>
                                                        <div class="coin">
                                                            <?php $currency_image = get_field( 'currency_image' ); ?>
                                                            <?php $country_image = get_field( 'country_image' ); ?>
                                                            <?php if ( $currency_image ) { ?>
                                                                <div class="round-coin-icon">
                                                                    <img src="<?php echo $currency_image['url']; ?>" alt="<?php echo $currency_image['alt']; ?>" />
                                                                </div>
                                                            <?php } elseif ($country_image) { ?>
                                                                <div class="round-coin-icon">
                                                                    <img src="<?php echo $country_image['url']; ?>" alt="<?php echo $country_image['alt']; ?>" />
                                                                </div>
                                                            <?php } ?>
                                                            <?php if ( get_field( 'global_crypto_name' ) && get_field( 'currency' ) ): ?>
                                                                <div>
                                                                    <h4><?php the_field( 'global_crypto_name' ) ?></h4><span><?php the_field( 'currency' ) ?></span>
                                                                </div>
                                                            <?php elseif ( get_field( 'global_country_name' ) ) : ?>
                                                                <div>
                                                                    <h4><?php the_field( 'global_country_name' ) ?></h4>
                                                                </div>
                                                            <?php else: ?>
                                                                <div>
                                                                    <h4><?php the_title(); ?></h4>
                                                                </div>
                                                            <?php endif; ?>
                                                            <a href="<?php the_permalink(); ?>" class="round-arrow-right"></a>
                                                        </div>
                                                    <?php endwhile; ?>
                                                    <?php wp_reset_postdata(); ?>
                                                    <?php wp_reset_query(); ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <!-- END Check If post object have multiple value -->

                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif ?>
                    <?php }
                     ?>

        </header>
        <!-- #masthead -->

        <div id="content" class="site-content">