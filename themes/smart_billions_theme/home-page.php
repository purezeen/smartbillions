<?php /* Template Name: Homepage */ get_header(); ?>


<main>
   <div class="home-hero">
		<div class="container">
			<div class="home-hero__wrap">
				<?php the_content(); ?>
			</div>
		</div>
   </div>

	<?php if ( have_rows( 'latest' ) ) : ?>
		<?php while ( have_rows( 'latest' ) ) : the_row(); ?>

			<div class="home-explore">
				<div class="container">

					<?php if ( get_sub_field( 'add_nofollow' ) == 1 ) { 
						$nofollow = "rel='nofollow'";
					} else { 
						$nofollow = "";
					} ?>

					<div class="home-explore__head">
						<?php the_sub_field( 'section_title' ); ?>
						<?php $cta = get_sub_field( 'cta' ); ?>
						<?php if ( $cta ) { ?>
							<a href="<?php echo $cta['url']; ?>" <?php echo $nofollow; ?> class="button" target="<?php echo $cta['target']; ?>"><?php echo $cta['title']; ?></a>
						<?php } ?>
					</div>

					<?php if ( have_rows( 'page_list' ) ) : ?>
						<div class="row">
							<?php while ( have_rows( 'page_list' ) ) : the_row(); ?>
								<div class="col-sm-6 col-xl-4">
									<div class="explore-item">

											<?php $choose_the_page = get_sub_field( 'choose_the_page' ); ?>
											<?php $page_title = ""; ?>
											<?php $page_url = ""; ?>
											<?php if ( $choose_the_page ) { ?>
												<?php $page_title = $choose_the_page['title']; ?>
												<?php $page_url =  $choose_the_page['url'];?>
												
											<?php } ?>

											<?php $icon = get_sub_field( 'icon' ); ?>
											<?php if ( $icon ) { ?>
												<a href="<?php echo $page_url; ?>">
													<div class="round-icon">
														<img src="<?php echo $icon['url']; ?>"
																alt="<?php echo $icon['alt']; ?>">
													</div>
												</a>
											<?php } ?>

											<div class="explore-content">
												<a href="<?php echo $page_url; ?>"><h3><?php echo $page_title; ?></h3></a>
													<?php the_sub_field( 'description' ); ?>
											</div>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>

				</div>
    		</div>

		<?php endwhile; ?>
	<?php endif; ?>

    <div class="home-recently">
        <div class="container">
            <h2>Recently published</h2>

				<?php if ( have_rows( 'last3' ) ) : ?>
					<div class="row">
						<?php while ( have_rows( 'last3' ) ) : the_row(); ?>
							<?php $post_object = get_sub_field( 'last_3' ); ?>
							<?php if ( $post_object ): ?>
								<?php $post = $post_object; ?>
								<?php setup_postdata( $post ); ?> 

									<div class="col-md-4">
										<div class="global-item">
												<span class="update update-desktop">Last update:
												<?php 
												$u_time = get_the_time('U'); 
												$u_modified_time = get_the_modified_time('U'); 
												if ($u_modified_time >= $u_time + 86400) { 
												the_modified_time('M j, Y'); 
												} else {
													echo get_the_date();
												}
												?></span>
												<a href="<?php the_permalink(); ?>">
												    <h3><?php the_title(); ?></h3>
							                    </a>
												<?php the_excerpt(); ?>
												<a href="<?php the_permalink(); ?>" class="button">Read more</a>
												<div class="update-mobile">
													<span class="update">Last update:</span>
													<span class="update">
													<?php 
													$u_time = get_the_time('U'); 
													$u_modified_time = get_the_modified_time('U'); 
													if ($u_modified_time >= $u_time + 86400) { 
													the_modified_time('M j, Y'); 
													} else {
														echo get_the_date();
													}
													?></span>
												</div>
										</div>
									</div>
								<?php wp_reset_postdata(); ?>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>

        </div>
    </div>

</main>

<?php get_footer(); ?>


