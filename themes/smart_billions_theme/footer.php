<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gulp-wordpress
 */

?>

</div><!-- #content -->

<footer class="footer">
   <div class="container">
      <div class="row">
         <div class="col-6 col-md-3 col-lg-2">
         <?php
            wp_nav_menu( array(
                'theme_location'    => 'footer1',
                'depth'             => 1,
                'container'         => false,
                'menu_class'        => ''
                )
            );
            ?>
         </div>
         <div class="col-6 col-md-3 col-lg-2">
            <?php
                wp_nav_menu( array(
                    'theme_location'    => 'footer2',
                    'depth'             => 1,
                    'container'         => false,
                    'menu_class'        => ''
                    )
                );
                ?>
         </div>
         <div class="col-6 col-md-3 col-lg-2 offset-lg-4">
            <a href="">
               <img src="<?php echo get_template_directory_uri() ?>/img/Logo-footer.svg" alt="Logo">
            </a>
         </div>
         <div class="col-6 col-md-3 col-lg-2">
            <ul class="copyright">
               <li>
                  Copyright 2021
               </li>
               <li>
                  <a href="SmartBillions.com">SmartBillions.com</a>
               </li>
               <li>
                  All rights reserved.
               </li>

            </ul>
         </div>

      </div>
   </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>