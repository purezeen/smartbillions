<?php if ( have_rows( 'best_of' ) ) : ?>
   <div class="col-lg-10 offset-lg-2 col-xl-9 offset-xl-2 best-of">
   <?php while ( have_rows( 'best_of' ) ) : the_row(); ?>
      <h2><?php the_sub_field( 'heading' ); ?></h2>
      <?php if ( have_rows( 'best_of_list' ) ) : ?>
      <ul class="info unstyle-list">
         <?php while ( have_rows( 'best_of_list' ) ) : the_row(); ?>
         <li> 
            <?php $logo = get_sub_field( 'logo' ); ?>

            <?php $logo_style = get_sub_field( 'logo_style' ); ?>
            <?php if($logo_style == "black"):?>
               <?php $logo_style_class = "round-icon--black"; ?>
               <?php elseif($logo_style == "without"): ?>
               <?php $logo_style_class = "round-icon--abra" ?>
               <?php elseif($logo_style == "default"): ?>
               <?php $logo_style_class = "" ?>
               <?php else: ?>
               <?php $logo_style_class = "" ?>
            <?php endif; ?>
            <?php if ( $logo ) { ?>
                  <div class="brand round-coin-icon <?php echo $logo_style_class; ?>"><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" /></div>
            <?php } ?>
            <div class="title-box">
               <h4><?php the_sub_field( 'name' ); ?></h4>
               <?php if(get_sub_field( 'rating' )): ?>
                  <div class="rating-wrap" style="--rating: <?php the_sub_field( 'rating' ); ?>;">
                     <div class="rating"></div>
                     <small><?php the_sub_field( 'rating' ); ?>/5</small>
                  </div>
               <?php endif; ?>
            </div>
            <?php $website_url = get_sub_field( 'website_url' ); ?>
            <a href="<?php //the_sub_field( 'website_url' ); ?>" class="brand-link" htef=""><?php //echo parse_url(get_sub_field( 'website_url' ), PHP_URL_HOST); ?></a>

            <?php if ( have_rows( 'more_info' ) ) : ?>
            <ol>
               <?php while ( have_rows( 'more_info' ) ) : the_row(); ?>
               <li>
                  <small><?php the_sub_field( 'label' ); ?></small> <?php the_sub_field( 'info' ); ?>
               </li>
               <?php endwhile; ?>
            </ol>
            <?php endif; ?>

            <?php if(get_sub_field( 'website_url' ) || get_sub_field( 'review_url' )): ?>
               <div class="buttons">

                  <?php if ( get_sub_field( 'add_nofollow_link'  ) == 1 ) { 
                     $nofollow = "rel='nofollow'";
                  } else { 
                     $nofollow = "";
                  } ?>
                  
                  <?php $website_url = get_sub_field( 'website_url' ); ?>
                  <?php if ( $website_url ) { ?>
                     <a href="<?php echo $website_url['url']; ?>" <?php echo $nofollow; ?> class="button" target="<?php echo $website_url['target']; ?>" ><?php echo $website_url['title']; ?></a>
                  <?php } ?>
                  
                  <?php if ( get_sub_field( 'add_nofollow_link2'  ) == 1 ) { 
                     $nofollow2 = "rel='nofollow'";
                  } else { 
                     $nofollow2 = "";
                  } ?>

                  <?php $review_url = get_sub_field( 'review_url' ); ?>
                  <?php if ( $review_url ) { ?>
                     <a href="<?php echo $review_url['url']; ?>" <?php echo $nofollow2; ?> class="button-neutral" target="<?php echo $review_url['target']; ?>"><?php echo $review_url['title']; ?></a>
                  <?php } ?>
               </div>
               <?php endif; ?>
         </li>
         <?php endwhile; ?>
      </ul>
      <?php endif; ?>
   <?php endwhile; ?>
   </div>
<?php endif; ?>