
<!-- Comparison table  -->
<div class="col-xl-10 offset-xl-1">
   <div class="crypto-table compare-table">
      <?php if(get_sub_field( 'table_title' )): ?>
         <p class="crypto-title"><?php the_sub_field( 'table_title' ); ?></p>
      <?php endif; ?>

      <table id="cryptoTable1" class="table" style="width:100%">
         <?php if ( have_rows( 'table_header' ) ) : ?>
            <thead>
               <tr>
                  <?php while ( have_rows( 'table_header' ) ) : the_row(); ?>
                     <th><?php the_sub_field( 'table_header_value' ); ?></th>
                  <?php endwhile; ?>
               </tr>
            </thead>
         <?php endif; ?>

         <tbody>
            <?php if ( have_rows( 'table_row' ) ) : ?>
               <?php while ( have_rows( 'table_row' ) ) : the_row(); ?>
                  <tr>
                     <?php if ( have_rows( 'table_column' ) ) : ?>
                        <?php while ( have_rows( 'table_column' ) ) : the_row(); ?>

                           <?php if ( get_sub_field( 'add_sticky_left_column' ) == 1 ) { 
                              ?>
                              <th>
                              <?php
                           } else { 
                              ?>
                              <td>
                              <?php
                           } ?>

                              <!-- Add check icon  -->
                              <?php if(get_sub_field( 'add_check_icon' ) == "check"): ?>
                                 <span class="check-icon"></span> 
                                 <?php elseif(get_sub_field( 'add_check_icon' ) == "uncheck"): ?>
                                    <span class="minus-icon"></span>
                                 <?php else: ?>
                                 <?php endif ?>
                              
                              <!-- Add table content  -->
                              <?php the_sub_field( 'table_column_value' ); ?>
                              
                              <!-- Add button and nofollow link -->
                              <?php $add_button = get_sub_field( 'add_button' ); ?>
                              <?php if ( get_sub_field( 'nofollow_link' ) == 1 ) { 
                                 $nofollow = "rel='nofollow'";
                              } else { 
                                 $nofollow = "";
                              } ?>
                              <?php if ( $add_button ) { ?>
                                 <a href="<?php echo $add_button['url']; ?>" <?php echo $nofollow; ?> target="<?php echo $add_button['target']; ?>"><?php echo $add_button['title']; ?></a>
                              <?php } ?>     
                              
                              <?php $add_icon_app_array = get_sub_field( 'add_icon_app' );
                              if ( $add_icon_app_array ):
                                 foreach ( $add_icon_app_array as $add_icon_app_item ):
                                       if($add_icon_app_item == "desktopapp"): ?>
                                             <div class="tooltip-wrap">
                                                <img src="<?php echo get_template_directory_uri() ?>/img/desktop-icon.png"
                                                   alt="Desktop app">
                                                <span class="tooltip">Desktop app</span>
                                             </div>

                                             <?php elseif($add_icon_app_item == "mobileapp"): ?>
                                                <div class="tooltip-wrap">
                                                   <img src="<?php echo get_template_directory_uri() ?>/img/mobile-icon.png"
                                                      alt="Mobile app">
                                                   <span class="tooltip">Mobile app</span>
                                                </div>
                                                
                                             <?php else:

                                       endif;
                                 endforeach;
                              endif; ?>
     
                           <?php if ( get_sub_field( 'add_sticky_left_column' ) == 1 ) { ?>
                           </th>
                           <?php } else { ?>
                           </td>
                           <?php } ?>
                           
                        <?php endwhile; ?>
                     <?php endif; ?>
                  </tr>
               <?php endwhile; ?>
            <?php endif; ?>
         </tbody>
      </table>
      
   </div>
</div>