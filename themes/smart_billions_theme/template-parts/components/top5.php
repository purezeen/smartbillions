
<?php if ( have_rows( 'top_five' ) ) : ?>
   <div class="rating-box">
      <?php while ( have_rows( 'top_five' ) ) : the_row(); ?>
            <div class="head">
               <h2><?php the_sub_field( 'heading' ); ?></h2>
               <?php if ( get_sub_field( 'icon' ) ) { ?>
                  <div class="round-icon">
                        <img src="<?php the_sub_field( 'icon' ); ?>" alt="Exchange-icon">
                  </div>
               <?php } ?>
            </div>
            
            <?php if ( have_rows( 'top_of_list' ) ) : ?>
               <ol>
               <?php while ( have_rows( 'top_of_list' ) ) : the_row(); ?>
                  <li> <?php the_sub_field( 'name' ); ?></li>
               <?php endwhile; ?>
               </ol>
            <?php endif; ?>
      <?php endwhile; ?>
   </div>
<?php endif; ?>