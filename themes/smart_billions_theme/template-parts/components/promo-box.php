<!-- Promo box  -->
<div class="rcmnd-item">
   <span class="rcmnd-item-label"><?php the_sub_field( 'label' ); ?></span>
   
      <div class="rcmnd-item-title"><?php the_sub_field( 'title' ); ?></div>

      <?php the_sub_field( 'description' ); ?>

      <?php if( get_sub_field( 'rating' )): ?>
         <div class="rating-wrap" style="--rating: <?php the_sub_field( 'rating' ); ?>;">
            <div class="rating"></div>
            <small><?php the_sub_field( 'rating' ); ?>/5</small>
         </div>
      <?php endif; ?>
   
   
      <?php if ( get_sub_field( 'add_no-follow_link' ) == 1 ) { 
         $nofollow_rel = "rel='nofollow'";
      } else { 
         $nofollow_rel = "";
      } ?>

      <?php $cta = get_sub_field( 'cta' ); ?>
      <?php if ( $cta ) { ?>
         <a href="<?php echo $cta['url']; ?>" <?php echo $nofollow_rel; ?> class="button" target="<?php echo $cta['target']; ?>"><?php echo $cta['title']; ?></a>
      <?php } ?>
</div>