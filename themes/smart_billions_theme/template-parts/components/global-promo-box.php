<!-- Global promo box  -->

<?php $post_object = get_sub_field( 'global_promo_box' ); ?>
<?php if ( $post_object ): ?>
   <?php $post = $post_object; ?>
   <?php setup_postdata( $post ); ?> 

      <?php if ( have_rows( 'promo_box' ) ) : ?>
         <div class="rcmnd-item">
            <?php while ( have_rows( 'promo_box' ) ) : the_row(); ?>
               <span class="rcmnd-item-label"><?php the_sub_field( 'label' ); ?></span>
               
               <div class="rcmnd-item-title"><?php the_title(); ?></div>

               <?php if ( get_sub_field( 'add_no-follow_link' ) == 1 ) { 
               ?>
                  <?php $no_follow_rel = "rel='nofollow'"; ?>
               <?php
               } else { ?>
                     <?php $no_follow_rel = ""; ?>
                  <?php
               } ?>
               <?php the_content(); ?>
               <?php if( get_sub_field( 'rating' )): ?>
               <div class="rating-wrap" style="--rating: <?php the_sub_field( 'rating' ); ?>;">
                  <div class="rating"></div>
                  <small><?php the_sub_field( 'rating' ); ?>/5</small>
               </div>

               <?php $cta = get_sub_field( 'cta' ); ?>
               <?php if ( $cta ) { ?>
                  <a href="<?php echo $cta['url']; ?>" <?php echo $no_follow_rel; ?> class="button" target="<?php echo $cta['target']; ?>"><?php echo $cta['title']; ?></a>
               <?php } ?>

               <?php endif; ?>

            <?php endwhile; ?>
         </div>
      <?php endif; ?>

   <?php wp_reset_postdata(); ?>
<?php endif; ?>




