
<div class="container">
   <div class="row">
      <div class="col-xl-8 offset-xl-2">
         <div class="author">
         <?php 
            global $post;
            $author = $post->post_author;
         
            $author_id = get_the_author_meta('ID');

            $author_name = get_the_author_meta( 'first_name', $author );
            
            $author_last_name = get_the_author_meta( 'last_name', $author );
         
            $author_description =  get_the_author_meta( 'description', $author );

            $userinfo = get_userdata( $author_id );
            $usermeta = get_user_meta( $author_id );
         ?>

            <div class="author-title">
               <?php echo get_avatar( $author_id, '185' ); ?>
               <h6><span>author</span> <?php echo $author_name; ?> <?php echo $author_last_name; ?></h6>
            </div>

            <p><?php echo $author_description; ?></p>

            <?php $linkedin = get_field( 'linkedin', 'user_'. $author_id ); ?>
            <?php if ($linkedin): ?>
            <!-- <span class="social-icon">
               <a href="<?php echo $linkedin; ?>" target="_blank"><img
                     src="<?php echo get_template_directory_uri(); ?>/img/linkedin-icon.svg" alt="linkedin"></a>
            </span> -->
            <?php endif ?>
            <?php
                  $meta_fields = array( 'facebook', 'twitter', 'google-plus', 'instagram', 'linkedin', 'pinterest', 'tumblr', 'youtube' );
                  foreach( $meta_fields as $meta) :

                     $this_meta = ( isset( $usermeta[$meta][0] ) && $usermeta[$meta][0] ) ? $usermeta[$meta][0] : '';
                     if( $this_meta ) :
                           echo '<span class="social-icon">';
                           echo '<a href="'.esc_url( $this_meta ).'" target="_blank">';
                           echo '<img src="'.get_template_directory_uri().'/img/'.esc_attr( $meta ).'-icon.svg" alt="'.esc_attr( $meta ).'">';
                           echo '</a></span>';
                     endif;

                  endforeach;
                  ?>
         </div>
      </div>
   </div>
</div>