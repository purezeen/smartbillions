<!-- Flexible content - left align -->
<?php function accordion_navigation() {
      if ( have_rows( 'content' ) ):
      $html = '';
      $html .= '<div class="col-xl-6 offset-xl-3"><div class="contents-list"><span>contents</span> <ul>';
      $p = 1; 
      while ( have_rows( 'content' ) ) : the_row(); 
      if ( get_row_layout() == 'content_heading_and_navigation_item' ) : ;
      $html .= '<li><a href="#';
      $html .=  $p;
      $html .= '">';
      $html .=  get_sub_field( 'content_heading' );
      $html .= '</a></li>';
      endif; 
      $p++; 
      endwhile;
      $html .= ' </ul> </div></div>';
      return $html;
   endif; 
} ?>

<?php $var = accordion_navigation(); ?>

<?php if ( have_rows( 'content' ) ): ?>

   <div class="container editor content">
      <div class="row">
            <?php $i = 1; ?>
            <?php while ( have_rows( 'content' ) ) : the_row(); ?>

            <?php if ( get_row_layout() == 'content_heading_and_navigation_item' ) : ?>
            <div class="col-xl-6 offset-xl-3">
               <h2 id="<?php echo $i; ?>"><?php the_sub_field( 'content_heading' ); ?></h2>
            </div>

            <?php elseif ( get_row_layout() == 'quick_links' ) : ?>
            <div class="col-xl-6 offset-xl-3">
               <?php if ( have_rows( 'quick_links' ) ) : ?>
                  <div class="buy-bitcoin">
                     <?php while ( have_rows( 'quick_links' ) ) : the_row(); ?>
                        <?php if ( get_sub_field( 'add_nofollow_link'  ) == 1 ) { 
                           $nofollow = "rel='nofollow'";
                        } else { 
                           $nofollow = "";
                        } ?>
                        <?php $marketplace_name = get_sub_field( 'marketplace_name' ); ?>
                           <?php if ( $marketplace_name ) { ?>
                              <a href="<?php echo $marketplace_name['url']; ?>" target="<?php echo $marketplace_name['target']; ?>" class="coin" <?php echo $nofollow; ?>>
                                 <div>
                                    <h2><?php echo $marketplace_name['title']; ?></h2>
                                 </div>
                                 <button class="round-arrow-right"></button>
                              </a>
                           <?php } ?>
                     <?php endwhile; ?>
                  </div>
               <?php endif; ?>
            </div>
          

            <?php elseif ( get_row_layout() == 'short_description' ) : ?>
            <div class="col-xl-6 offset-xl-3">
               <p><?php the_sub_field( 'text_area' ); ?></p>
            </div>

            <?php elseif ( get_row_layout() == 'full_text_editor' ) : ?>
            <div class="col-xl-6 offset-xl-3">
               <?php the_sub_field( 'text_editor' ); ?>
            </div>

            <!-- Promo box  -->
            <?php elseif ( get_row_layout() == 'call_for_an_action_promo_block' ) : ?>
            <?php if ( get_sub_field( 'add_custom_promo_box' ) == 1 ) { ?>
               <?php if ( have_rows( 'custom_promo_box' ) ) : ?>
				      <?php while ( have_rows( 'custom_promo_box' ) ) : the_row(); ?>
                     <div class="col-xl-6 offset-xl-3">
                        <?php get_template_part( 'template-parts/components/promo', 'box' ); ?>
                     </div>
                  <?php endwhile; ?>
			      <?php endif; ?>
               <?php
            } else { 
               ?>
               <div class="col-xl-6 offset-xl-3">
                  <?php get_template_part( 'template-parts/components/global-promo', 'box' ); ?>
               </div>
               <?php
            } ?>
            <!-- End promo box  -->

            <?php elseif  ( get_row_layout() == 'table' ) : ?>
               <div class="col-xl-8 offset-xl-2">
                  <div class="crypto-table">
                     <p class="crypto-title"><?php the_sub_field( 'table_title' ); ?></p>
                     <?php the_sub_field( 'text_editor' ); ?>
                  </div>
               </div>

            <?php elseif ( get_row_layout() == 'bigger_text' ) : ?>
               <div class="col-xl-8 offset-xl-2">
                  <p class="bigger-text"><?php the_sub_field( 'text' ); ?></p>
               </div>

            <?php elseif ( get_row_layout() == 'full_with_image' ) : ?>
               <?php if ( get_sub_field( 'select_image' ) ) { ?>
                  <div class="col-xl-10 offset-xl-1">
                     <img class="img-intro" src="<?php the_sub_field( 'select_image' ); ?>" alt="News-img">
                  </div>
               <?php } ?>

            <?php elseif ( get_row_layout() == 'add_navigation_item_scroll_to' ) : ?>
               <?php if($var) {
                  echo $var;
               } ?>

            <?php elseif ( get_row_layout() == 'Pros and Cons' ) : ?>
               <!-- Include pros and cons section  -->
               <div class="col-xl-6 offset-xl-3">
                  <?php if ( have_rows( 'pros_&_cons' ) ) : ?>
                     <?php while ( have_rows( 'pros_&_cons' ) ) : the_row(); ?>
                        <?php if(get_sub_field( 'pros' ) || get_sub_field( 'info' )) : ?>
                              <div class="pros-cons">
                                 <!-- <div class="container"> -->
                                    <div class="row">
                                          <div class="col-md-6">
                                             <div class="pros">
                                                <h3><span class="icon"></span>Pros</h3>
                                                <?php the_sub_field( 'pros' ); ?>
                                             </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="cons">
                                                <h3><span class="icon"></span>Cons</h3>
                                                <?php the_sub_field( 'cons' ); ?>
                                             </div>
                                          </div>
                                    </div>
                                 <!-- </div> -->
                              </div>
                        <?php endif; ?>
                     <?php endwhile; ?>
                  <?php endif; ?>
                  </div>
               <!-- End pros and cons section  -->

            <?php elseif ( get_row_layout() == 'best_of' ) : ?>
               <!-- Include best of  -->
               <?php get_template_part( 'template-parts/components/best', 'of' ); ?>
               <!-- End best of  -->

            <?php elseif ( get_row_layout() == 'top_5' ) : ?>
               <!-- Include best of  -->
               <div class="col-xl-6 offset-xl-3">
                  <?php get_template_part( 'template-parts/components/top5' ); ?>
               </div>
               <!-- End best of  -->

            <?php elseif ( get_row_layout() == 'comparison_chart' ) : ?>
            <!-- Comparison chart  -->
            <?php get_template_part( 'template-parts/components/comparison', 'chart' ); ?>

            <?php endif; ?>
            <?php $i++; ?>
            <?php endwhile; ?>
         </div>

      </div>
   </div>
<?php endif; ?>

<!-- Flexible content for Accordion only -->
<?php if ( have_rows( 'content' ) ): ?>
<?php while ( have_rows( 'content' ) ) : the_row(); ?>
<?php if ( get_row_layout() == 'accordion' ) : ?>
<div class="accordion">
   <div class="container">
      <div class="row">
         <div class="col-xl-6 offset-xl-2">
            <div class="accordion-title">
               <div class="round-question-mark">
                  <img src="<?php echo get_template_directory_uri() ?>/img/question-mark.svg" alt="question-mark">
               </div>
               <h3 id="<?php the_sub_field( 'value_of_scroll_to_from_content_navigation' ); ?>">
                  <?php the_sub_field( 'content_heading' ); ?></h3>
            </div>
            <div>
               <?php if ( have_rows( 'accordion' ) ) : ?>
               <?php while ( have_rows( 'accordion' ) ) : the_row(); ?>
               <div class="accordion-item">
                  <button><?php the_sub_field( 'accordion_title' ); ?><span class="round-arrow-down"></span></button>
                  <div class="accordion-content">
                     <p><?php the_sub_field( 'accordion_description' ); ?></p>
                  </div>
               </div>
               <?php endwhile; ?>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </div>
</div>
<?php endif; ?>
<?php endwhile; ?>
<?php endif; ?>
<!-- END Flexible content for Accordion only -->

