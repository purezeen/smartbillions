
<div class="container">
   <div class="row">
      <div class="col-xl-8 offset-xl-2">
         <?php
               // Get all pages under parent page
               $pagelist = get_pages( array( 
                  'child_of'     => $post->post_parent
               ));
               $pages = array();
               foreach ($pagelist as $page) {
                  $pages[] += $page->ID;
               }
               // Check current page array number
               $current = array_search(get_the_ID(), $pages);

               // check if page has next page in hierarchy 
               if (!empty($pages[$current+1])) { 
                  // Get next page in array
                  $nextID = $pages[$current+1]; ?>
                  <div class="next-article">
                     <p>Next article</p>
                     <h3><?php echo get_the_title($nextID); ?></h3>
                     <a href="<?php echo get_permalink($nextID); ?>" class="button-neutral">Read article</a>
                  </div>
               <?php } ?>
               
      </div>
   </div>
</div>