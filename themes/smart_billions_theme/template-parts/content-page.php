<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */
		global $page_ID;
		global $content;
		echo '<h1>' . get_the_title($page_ID) . '</h1>';
		echo $content;
