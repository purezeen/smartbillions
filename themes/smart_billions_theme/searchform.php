
<div class="col-6 col-md-4 col-lg-3 col-xl-2 order-xl-12">
    <form class="search" method="get" action="<?php echo esc_url( home_url() ); ?>">
        <input type="search" placeholder="Search" name="s">
        <i class="icon-search"></i>
        <!-- <img src="<?php echo get_template_directory_uri() ?>/img/Search-icon.svg" alt="Search-icon"> -->
    </form>
    <!-- search form -->
</div>