<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package gulp-wordpress
 */

get_header(); ?>

	<div class="page-404">
		<div class="container">
			<div class="row">
				<div class="col-xl-7 offset-xl-2">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'gulp-wordpress' ); ?></h1>
				</div>
			</div>
		</div>
	</div>
	 
<?php
get_footer();
