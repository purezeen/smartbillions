<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gulp-wordpress
 */

get_header(); ?>

<?php get_header(); ?>

<main class="best-exchange">
    <div class="news-head wallets-head">
        <div class="container editor">
            <div class="row">
                <div class="col-xl-6 offset-xl-2">
                    <?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
                    <span class="update update-desktop update-page">Last update:
                    <?php 
                    $u_time = get_the_time('U'); 
                    $u_modified_time = get_the_modified_time('U'); 
                    if ($u_modified_time >= $u_time + 86400) { 
                    the_modified_time('M j, Y'); 
                    } else {
                        echo get_the_date();
                    }
                    ?></span>
                </div>
                <div class="col-xl-6 offset-xl-2">
                    <h1><?php the_title(); ?></h1>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Include flexible single content  -->
    <?php get_template_part( 'template-parts/flexible-single', 'leftalign' ); ?>
    <!-- End flexible single content  -->    
    
</main>
<?php get_footer(); ?>
	

<?php
get_footer();
