<?php
/**
 * gulp-wordpress functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gulp-wordpress
 */

if ( ! function_exists( 'gulp_wordpress_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function gulp_wordpress_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on gulp-wordpress, use a find and replace
	 * to change 'gulp-wordpress' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'gulp-wordpress', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'gulp-wordpress' ),
		'footer1' => esc_html__( 'Footer1', 'gulp-wordpress' ),
		'footer2' => esc_html__( 'Footer2', 'gulp-wordpress' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'gulp_wordpress_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'gulp_wordpress_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function gulp_wordpress_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'gulp_wordpress_content_width', 640 );
}
add_action( 'after_setup_theme', 'gulp_wordpress_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function gulp_wordpress_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'gulp-wordpress' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'gulp-wordpress' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'gulp_wordpress_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function gulp_wordpress_scripts() {

	wp_enqueue_style( 'gulp-wordpress-style', get_stylesheet_uri(), array(), '1.345' );

	wp_enqueue_style( 'slick-css',  get_template_directory_uri() . '/css/slick.css' );
	// wp_enqueue_style( 'data-tables-css',  get_template_directory_uri() . '/css/jquery.dataTables.min.css' );
	// wp_enqueue_style( 'fixed-columns-css',  get_template_directory_uri() . '/css/fixedColumns.dataTables.min.css' );
	wp_enqueue_style( 'scroll-pane-css',  get_template_directory_uri() . '/css/jquery.jscrollpane.css' );
	wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '1', true );
	wp_enqueue_script( 'data-tables-js', get_template_directory_uri() . '/js/jquery.dataTables.min.js', array('jquery'), '1', true );
	wp_enqueue_script( 'fixed-columns-js', get_template_directory_uri() . '/js/dataTables.fixedColumns.min.js', array('jquery'), '1', true );
	wp_enqueue_script( 'jscrollpane-js', get_template_directory_uri() . '/js/jquery.jscrollpane.js', array('jquery'), '1', true );
	wp_enqueue_script( 'mousewheel-js', get_template_directory_uri() . '/js/jquery.mousewheel.js', array('jquery'), '1', true );
	wp_enqueue_script( 'mwheellntent-js', get_template_directory_uri() . '/js/mwheellntent.js', array('jquery'), '1', true );

	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1', true );


	wp_enqueue_script('ajax', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1', true);
	wp_localize_script('ajax', 'wpAjax', 
	array('ajaxUrl' => admin_url('admin-ajax.php'))
	);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'gulp_wordpress_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


// Register Custom Post Type
function cpt_promobox() {

	$labels = array(
		'name'                  => _x( 'Promo box', 'Job General Name', 'gulp_wordpress' ),
		'singular_name'         => _x( 'Promo box', 'Job Singular Name', 'gulp_wordpress' ),
		'menu_name'             => __( 'Promo box', 'gulp_wordpress' ),
		'name_admin_bar'        => __( 'Promo box', 'gulp_wordpress' ),
		'archives'              => __( 'Item Archives', 'gulp_wordpress' ),
		'attributes'            => __( 'Item Attributes', 'gulp_wordpress' ),
		'parent_item_colon'     => __( 'Parent Item:', 'gulp_wordpress' ),
		'all_items'             => __( 'All Items', 'gulp_wordpress' ),
		'add_new_item'          => __( 'Add New Item', 'gulp_wordpress' ),
		'add_new'               => __( 'Add New', 'gulp_wordpress' ),
		'new_item'              => __( 'New Item', 'gulp_wordpress' ),
		'edit_item'             => __( 'Edit Item', 'gulp_wordpress' ),
		'update_item'           => __( 'Update Item', 'gulp_wordpress' ),
		'view_item'             => __( 'View Item', 'gulp_wordpress' ),
		'view_items'            => __( 'View Items', 'gulp_wordpress' ),
		'search_items'          => __( 'Search Item', 'gulp_wordpress' ),
		'not_found'             => __( 'Not found', 'gulp_wordpress' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'gulp_wordpress' ),
		'featured_image'        => __( 'Featured Image', 'gulp_wordpress' ),
		'set_featured_image'    => __( 'Set featured image', 'gulp_wordpress' ),
		'remove_featured_image' => __( 'Remove featured image', 'gulp_wordpress' ),
		'use_featured_image'    => __( 'Use as featured image', 'gulp_wordpress' ),
		'insert_into_item'      => __( 'Insert into item', 'gulp_wordpress' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'gulp_wordpress' ),
		'items_list'            => __( 'Items list', 'gulp_wordpress' ),
		'items_list_navigation' => __( 'Items list navigation', 'gulp_wordpress' ),
		'filter_items_list'     => __( 'Filter items list', 'gulp_wordpress' ),
	);
	$args = array(
		'label'                 => __( 'Promo box', 'gulp_wordpress' ),
		'description'           => __( 'Promo box  Description', 'gulp_wordpress' ),
		'public' => true,
		'exclude_from_search' => true,
		'show_in_admin_bar'   => false,
		'show_in_nav_menus'   => false,
		'publicly_queryable'  => false,
		'query_var'           => false
	);
	register_post_type( 'Promo box', $args );

}

add_action( 'init', 'cpt_promobox', 0 );


//* ======== EXCERTP ======== *//
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


//* =========== ACF ============ */

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
		
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme contact Settings',
		'menu_title'	=> 'Contact',
		'parent_slug'	=> 'theme-general-settings',
	));
}

// Adding Previous and Next Post Links
function pagination_bar() {
	global $wp_query;

	$total_pages = $wp_query->max_num_pages;
  
	if ($total_pages > 1) {
		 $current_page = max(1, get_query_var('paged'));
			 $big = 999999999; // need an unlikely integer
				if ( $current_page == 1) echo '<a class="prev page-numbers disabled">Prethodna</a>';
		 echo paginate_links(array(
			  'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				 'format'  => '?paged=%#%',
				 'current' => $current_page,
			  'total' => $total_pages,
			  'prev_next' => true,
			  'end_size' => 1,
			  'mid_size' => 1,
			  'prev_text' => __('Prethodna'),
			  'next_text' => __('Sledeća'),
		 ));
		 if ( $current_page == $wp_query->max_num_pages ) echo '<a class="next page-numbers disabled">Sledeća</a>';
	}
	echo '<style>.page-numbers.disabled{opacity:.2;}.page-numbers.disabled:hover{color:#000;}</style>'; 
}

// ACF Custom Blocks **************************************************************************************
function register_acf_block_types() {
   // register a testimonial block.
   acf_register_block_type(array(
      'name'              => 'testimonial',
      'title'             => __('Testimonial'),
      'description'       => __('A custom testimonial block.'),
      'render_template'   => 'template-parts/blocks/testimonial/testimonial.php',
      'category'          => 'formatting',
      'icon'              => 'admin-comments',
      'keywords'          => array( 'testimonial', 'quote' ),
   ));
}
// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
   add_action('acf/init', 'register_acf_block_types');
}


// use Parent Category Template for child in WordPress
function new_subcategory_hierarchy() { 
	$category = get_queried_object();

	$parent_id = $category->category_parent;

	$templates = array();

	if ( $parent_id == 0 ) {
		 // Use default values from get_category_template()
		 $templates[] = "category-{$category->slug}.php";
		 $templates[] = "category-{$category->term_id}.php";
		 $templates[] = 'category.php';     
	} else {
		 // Create replacement $templates array
		 $parent = get_category( $parent_id );

		 // Current first
		 $templates[] = "category-{$category->slug}.php";
		 $templates[] = "category-{$category->term_id}.php";

		 // Parent second
		 $templates[] = "category-{$parent->slug}.php";
		 $templates[] = "category-{$parent->term_id}.php";
		 $templates[] = 'category.php'; 
	}
	return locate_template( $templates );
}

add_filter( 'category_template', 'new_subcategory_hierarchy' );


// Add Categories and Tags to Pages in WordPress
function add_taxonomies_to_pages() {
	register_taxonomy_for_object_type( 'post_tag', 'page' );
	register_taxonomy_for_object_type( 'category', 'page' );
	}
  add_action( 'init', 'add_taxonomies_to_pages' );

  function category_and_tag_archives( $wp_query ) {
	$my_post_array = array('post','page');
	 
	 if ( $wp_query->get( 'category_name' ) || $wp_query->get( 'cat' ) )
	 $wp_query->set( 'post_type', $my_post_array );
	 
	 if ( $wp_query->get( 'tag' ) )
	 $wp_query->set( 'post_type', $my_post_array );
	}
	
	add_post_type_support( 'page', 'excerpt' );

	// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination() {
    global $wp_query;
    $big = 999999999;
    echo paginate_links( array(
        'base'    => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
        'format'  => '?paged=%#%',
        'current' => max( 1, get_query_var( 'paged' ) ),
        'total'   => $wp_query->max_num_pages,
        'next_text' => '',
        'prev_text' => ''
    ) );
}

add_action('pre_get_posts', 'ci_paging_request');
function ci_paging_request($wp) {
	//We don't want to mess with the admin panel.
	if(is_admin()) return;

	$num = get_option('posts_per_page', 15);

	if( is_search() )
		$num = 4; 

	if( ! isset( $wp->query_vars['posts_per_page'] ) and $wp->is_main_query() )
	{
		$wp->query_vars['posts_per_page'] = $num;
	}
}

add_action( 'wp_ajax_nopriv_filter', 'filter_ajax' );
add_action( 'wp_ajax_filter', 'filter_ajax' );

function filter_ajax() {
	?>
	<div class="compare-buttons">
		<a href="/crypto/compare/" class="button">new comparison</a>
		<a href="/crypto/compare/" class="button-neutral ">view all</a>
	</div>
	<?php

	$category = $_POST['category'];
	$child = $_POST['child'];

	$args = array(
		'post_type'   => 'page',
		'posts_per_page' => -1,
		);

		if(isset($category)){
			$args['category__in'] = array($category);
		};

		$wp_query = new WP_Query($args); ?>

		<?php if ($wp_query->have_posts()) : ?>

			<div class="comparison">
				<div class="caption">
				<?php 
					if(isset($child )) { ?>
					<?php  
						$parentid = get_category($category );
						$parentid = $parentid->term_id; // get the object for the catid
					?>

					<?php
					}else { ?>
						<?php  
						$cat = get_category($category ); // get the object for the catid
						$parentid = $cat->category_parent; // assign parent ID (if exists) to $catid
					?>
					<?php
					}
					?>

					<h2><?php echo get_cat_name($parentid);?><span><?php echo get_cat_name( $category);?></span></h2>
					
					<?php
					// Define taxonomy prefix
					// Replace NULL with the name of the taxonomy eg 'category'
					$taxonomy_prefix = 'category';

					// Define term ID
					// Replace NULL with ID of term to be queried eg '123'
					$term_id = $parentid;

					// Define prefixed term ID
					$term_id_prefixed = $taxonomy_prefix .'_'. $term_id; ?>

					<?php if ( have_rows( 'category', $term_id_prefixed ) ) : ?>
						<?php while ( have_rows( 'category', $term_id_prefixed ) ) : the_row(); ?>
							<?php $icon = get_sub_field( 'icon', $term_id_prefixed ); ?>
							<?php if ( $icon ) { ?>
								<div class="round-icon">
										<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
								</div>
							<?php } ?>
							
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			
				<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<a href="<?php the_permalink(); ?>">
						<?php the_title('<h3>','</h3>') ?>
					</a>
					
				<?php endwhile; ?>
				
			</div>
			<?php wp_reset_postdata(); ?>
		<?php endif; 

	die();
}

// ACF CUSTOM ADMIN STYLE **************************************************************************************
function my_acf_admin_head() {
	?>
	<style type="text/css">
		.acf-flexible-content .layout .acf-fc-layout-handle {
			/*background-color: #00B8E4;*/
			background-color: #3345A3;
			color: #eee;
		}
		.acf-repeater.-row > table > tbody > tr > td,
		.acf-repeater.-block > table > tbody > tr > td {
			border-top: 2px solid #3345A3;
		}
		.acf-repeater .acf-row-handle {
			vertical-align: top !important;
			padding-top: 16px;
		}
		.acf-repeater .acf-row-handle span {
			font-size: 20px;
			font-weight: bold;
			color: #3345A3;
		}
		.imageUpload img {
			width: 75px;
		}
		.acf-repeater .acf-row-handle .acf-icon.-minus {
			top: 30px;
		}
	</style>
	<?php
	}
	add_action('acf/input/admin_head', 'my_acf_admin_head');


