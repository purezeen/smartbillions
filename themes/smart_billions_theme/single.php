<?php get_header(); ?>

<main class="single-news">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 offset-xl-2">
					<?php if (function_exists('rank_math_the_breadcrumbs')) rank_math_the_breadcrumbs(); ?>
					  <span class="update update-desktop update-page">Last update:
							<?php 
							$u_time = get_the_time('U'); 
							$u_modified_time = get_the_modified_time('U'); 
							if ($u_modified_time >= $u_time + 86400) { 
							the_modified_time('M j, Y'); 
							} else {
								echo get_the_date();
							}
							?></span>
                <h1><?php the_title(); ?></h1>
            </div>
        </div>

			<?php if ( has_post_thumbnail() ) { ?>
				<div class="row">
					<div class="col-xl-10 offset-xl-1">
						<?php the_post_thumbnail('',['class' => 'img-intro', 'alt' => 'Feature image']); ?>
					</div>
				</div>
          <?php } ?>
		</div>
			
		<!-- Include flexible single content  -->
		<?php get_template_part( 'template-parts/flexible-single', 'centeralign' ); ?>
		<!-- End flexible single content  -->
		
</main>
<?php get_footer(); ?>