<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package gulp-wordpress
 */

get_header(); ?>
<main class="search-results">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">

							<?php
							if ( have_posts() ) : ?>

								<h1 class="page-title"><strong>Search Results for:</strong><?php printf( esc_html__( ' %s', 'gulp-wordpress' ),  get_search_query() ); ?></h1>

								<?php
								/* Start the Loop */
								while ( have_posts() ) : the_post();

									/**
									 * Run the loop for the search to output the results.
									 * If you want to overload this in a child theme then include a file
									 * called content-search.php and that will be used instead.
									 */
									get_template_part( 'template-parts/content', 'search' );

								endwhile; ?>

	            <!-- pagination -->
	            <div class="pagination">
	              <?php html5wp_pagination(); ?>
	            </div>
	            <!-- /pagination -->

							<?php else :

								get_template_part( 'template-parts/content', 'none' );

							endif; ?>

            </div>
        </div>
    </div>
</main>
<?php get_footer();
